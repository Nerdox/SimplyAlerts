local self = SimplyAlerts
local L, SM = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true), LibStub("LibSharedMedia-3.0")

function tableCopy(obj)
    if type(obj) ~= 'table' then return obj end
    local res = setmetatable({}, getmetatable(obj))
    for k, v in pairs(obj) do res[tableCopy(k)] = tableCopy(v) end
    return res
end

function SimplyAlerts:GetDefines()
    self.defaults = {
        profile = {
            chat="party",localmsgs=true,chatmsgs=true,controls=true,autorbg=false,licon=true,licsiz=10,autoCancelSoulFragment={["enabled"] = false, ["notify"] = false},
            classframes = { ["enabled"] = false, ["frames"] = "all", ["mode"] = "all", ["translucency"] = { 
                ["health"] = { ["player"] = { ["disabled"] = false, ["val"] = 0.7}, ["other"] = { ["disabled"] = false, ["val"] = 0.7} },
                ["name"] = { ["player"] = { ["disabled"] = false, ["val"] = 0.7}, ["other"] = { ["disabled"] = false, ["val"] = 0.7} } 
            } },
            statusBars = { ["enabled"] = false, ["updateDisplay"] = false, ["barTypes"] = { 
                -- power types: 6 - runic power | 3 - energy | 2 - focus (hunter pet) | 1 - rage | 0 - mana
                ["player"] = { ["enabled"] = true, ["powers"] = {[0] = true, [1] = false, [2] = false, [3] = true, [6] = true}, ["offPerc"] = 50, ["offPower"] = 50 },
                ["pet"] = { ["enabled"] = true, ["powers"] = {[0] = true, [1] = false, [2] = false, [3] = true, [6] = true}, ["offPerc"] = 35, ["offPower"] = 33 }, 
                ["party"] = { ["enabled"] = true, ["powers"] = {[0] = true, [1] = false, [2] = false, [3] = true, [6] = true}, ["offPerc"] = 37, ["offPower"] = 31 },
                ["target"] = { ["enabled"] = true, ["powers"] = {[0] = true, [1] = false, [2] = false, [3] = true, [6] = true}, ["offPerc"] = 50, ["offPower"] = 50 }
            } },
            markers = { ["marker"] = true, ["petmark"] = true, ["units"] = { 
                ["player"]=1,["player-pet"]=2, ["party1"]=3,["party1-pet"]=4, ["party2"]=5,["party2-pet"]=6, ["party3"]=7,["party3-pet"]=8, ["party4"]=0 }
            },
            dodger = { ["enabled"] = false, ["ignoreafk"] = true, note = { ["enabled"] = false, ["string"] = "dodge" } },
            messages = { ["lmsgsds"] = "!!! %s DISPELLED !!!", ["lmsgsdw"] = "!!! %s DOWN !!!", ["gmsgup"] = "{MOON} %s USED {MOON}", ["gmsgdw"] = "{CROSS} %s DOWN {CROSS}" },
            uppercase = { ["local"] = "dwonly", ["chat"] = "bothe" },
            spellstatus = { ["enabled"] = true, ["type"] = "me", ["reporter"] = { ["enabled"] = true, ["types"] = { ["enemy"] = true, ["me"] = true }, ["chat"] = "me" },
            filter = { ["ABSORB"]=false,["BLOCK"]=true,["DEFLECT"]=true,["DODGE"]=true,["EVADE"]=true,["IMMUNE"]=true,["MISS"]=true,["PARRY"]=true,["REFLECT"]=true,["RESIST"]=true } },
            onlyarena = { ["chat"] = false, ["local"] = false, ["spell"] = true, ["reminder"] = true },
            reminder = { ["enabled"] = true, ["spells"] = { 47436 }, ["interval"] = 30 },
            Spellzp = { 22812, 29166, 54428, 64205 }, -- messages to group
            Spellz = { 29166, 22812, 20217, 48936, 54428, 64205, 47436, 47440 }, -- messages for player
            alert = { ["pos"] = {"CENTER",nil,"CENTER",0,190}, ["font"] = SM:GetDefault("font"), ["fontsize"] = 40, ["holdtime"] = 1, ["lock"] = false },
            bars = { ["lock"] = false, ["global"] = false, ["showtext"] = true,
            barst={["player"]={["cache"]={},["enabled"]=true,["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["party1"]={["enabled"]=false,["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["party2"]={["enabled"]=false,["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["party3"]={["enabled"]=false,["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["party4"]={["enabled"]=false,["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["arena1"]={["enabled"]=false, ["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["arena2"]={["enabled"]=false, ["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["arena3"]={["enabled"]=false, ["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["arena4"]={["enabled"]=false, ["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false},
                ["arena5"]={["enabled"]=false, ["font"] = SM:GetDefault("font"), ["fontsize"] = 18, ["iconsize"] = 30, ["iconmargin"] = 5, ["bonuses"] = false}}
            }
        }
    }

    self.SpellCon = {
        -- Spell ID                 TEXT                            SPELL NAME
        [42950]        = "{SKULL} DRAGON'S BREATHED {SKULL}",   -- Dragon's Breath
        [51724]        = "{SKULL} SAPPED {SKULL}",              -- Sap
        [1776]         = "{SKULL} GOUG'D {SKULL}",              -- Gouge
        [51514]        = "{SKULL} HEX'D {SKULL}",               -- Hex
        [12826]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph
        [28271]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Turtle)
        [28272]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Pig)
        [61025]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Serpent)
        [61305]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Black Cat)
        [61721]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Rabbit)
        [61780]        = "{SKULL} POLYMORPHED {SKULL}",         -- Polymorph (Turkey)
        [14309]        = "{SKULL} TRAPPED {SKULL}",             -- Freezing Trap
        [60210]        = "{SKULL} TRAPPED {SKULL}",             -- Freezing Arrow
        [49012]        = "{SKULL} DISORIENTED {SKULL}",         -- Wyvern Sting
        [20066]        = "{SKULL} REPETANCED {SKULL}",          -- Repentance
        
        [15487]        = "{CIRCLE} SILENCED {CIRCLE}",          -- Silence 
        [18469]        = "{CIRCLE} SILENCED {CIRCLE}",          -- Improved Counterspell R1
        [55021]        = "{CIRCLE} SILENCED {CIRCLE}",          -- Improved Counterspell R2
        [19647]        = "{CIRCLE} SILENCED {CIRCLE}",          -- Spell Lock
        [49916]        = "{CIRCLE} SILENCED {CIRCLE}",          -- Strangulate
        
        [51722]        = "{SQUARE} DISARMED {SQUARE}",          -- Dismantle
        [676]          = "{SQUARE} DISARMED {SQUARE}",          -- Disarm
        [64058]        = "{SQUARE} DISARMED {SQUARE}",          -- Psychic Horror
        
        [2094]         = "{DIAMOND} BLINDED {DIAMOND}",         -- Blind
        [6215]         = "{DIAMOND} FEARED {DIAMOND}",          -- Fear
        [6358]         = "{DIAMOND} SEDUCED {DIAMOND}",         -- Seducation
        [17928]        = "{DIAMOND} FEARED {DIAMOND}",          -- Howl of Terror
        [10890]        = "{DIAMOND} FEARED {DIAMOND}",          -- Psychic Scream
        [5246]         = "{DIAMOND} FEARED {DIAMOND}",          -- Intimidating Shout
        
        [12809]        = "{TRIANGLE} STUNNED {TRIANGLE}",       -- Concussion Blow
        [46968]        = "{TRIANGLE} SHOCKWAVED {TRIANGLE}",    -- Shockwave
        [10308]        = "{TRIANGLE} HOJ'D {TRIANGLE}",         -- Hammer of Justice
        [8983]         = "{TRIANGLE} BASH'D {TRIANGLE}",        -- Bash
        [8643]         = "{TRIANGLE} STUNNED {TRIANGLE}",       -- Kidney Shot
        [20549]        = "{TRIANGLE} STUNNED {TRIANGLE}",       -- War Stomp
        [44572]        = "{TRIANGLE} DEEP FREEZED {TRIANGLE}",  -- Deep Freeze
        [30414]        = "{TRIANGLE} STUNNED {TRIANGLE}",       -- Shadowfury
        
        [33395]        = "{MOON} FREEZED {MOON}",               -- Freeze
        [53548]        = "{MOON} PINNED {MOON}",                -- Pin
        [53308]        = "{MOON} ROOTED {MOON}",                -- Entangling Roots
        [53313]        = "{MOON} ROOTED {MOON}",                -- Natures Grasp
        [8377]         = "{MOON} ROOTED {MOON}",                -- Earthgrab
        
        [47860]        = "{CROSS} DEATH COILED {CROSS}",        -- Death Coil
        [605]          = "{CROSS} MIND CONTROLED {CROSS}",      -- Mind Control
        [18647]        = "{CROSS} BANISHED {CROSS}",            -- Banish
        [18658]        = "{CROSS} HIBERNATED {CROSS}",          -- Hibernate
        [33786]        = "{CROSS} CYCLONED {CROSS}",            -- Cyclone
    }

    self.ItemProcs = {
        -- Proc ID             Item ID             CD                                                       Item Name
        [71561]        = { ["item"] = {50363}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (T) (HC)
        [71484]        = { ["item"] = {50362}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (T) (N)
        [71560]        = { ["item"] = {50363}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (V) (HC)
        [71492]        = { ["item"] = {50362}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (V) (N)
        [71559]        = { ["item"] = {50363}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (D) (HC)
        [71491]        = { ["item"] = {50362}, ["icd"] = 105, ["spec"] = 0 },                    -- Deathbringer's Will (D) (N)
        [88888]        = { ["item"] = {51377,42124,51378,42126}, ["icd"] = 120, ["spec"] = 0 },  -- Medallion of the Alliance & Horde
        [71607]        = { ["item"] = {50354,50726}, ["icd"] = 120, ["spec"] = 0 },              -- Bauble of True Blood (HC/N)
        [71636]        = { ["item"] = {50365}, ["icd"] = 90, ["spec"] = 0 },                     -- Phylactery of the Nameless Lich (HC)
        [71605]        = { ["item"] = {50360}, ["icd"] = 90, ["spec"] = 0 },                     -- Phylactery of the Nameless Lich (N)
        [75456]        = { ["item"] = {54590}, ["icd"] = 45, ["spec"] = 0 },                     -- Sharpened Twilight Scale (HC)
        [75458]        = { ["item"] = {54569}, ["icd"] = 45, ["spec"] = 0 },                     -- Sharpened Twilight Scale (N)
        [75480]        = { ["item"] = {54591}, ["icd"] = 45, ["spec"] = 0 },                     -- Petrified Twilight Scale (HC)
        [75477]        = { ["item"] = {54571}, ["icd"] = 45, ["spec"] = 0 },                     -- Petrified Twilight Scale (N)
        [75473]        = { ["item"] = {54588}, ["icd"] = 45, ["spec"] = 0 },                     -- Charred Twilight Scale (HC)
        [75466]        = { ["item"] = {54572}, ["icd"] = 45, ["spec"] = 0 },                     -- Charred Twilight Scale (N)
        [71644]        = { ["item"] = {50348}, ["icd"] = 45, ["spec"] = 0 },                     -- Dislodged Foreign Object (HC)
        [71601]        = { ["item"] = {50353}, ["icd"] = 45, ["spec"] = 0 },                     -- Dislodged Foreign Object (N)
        [71541]        = { ["item"] = {50343}, ["icd"] = 45, ["spec"] = 0 },                     -- Whispering Fanged Skull (HC)
        [71401]        = { ["item"] = {50342}, ["icd"] = 45, ["spec"] = 0 },                     -- Whispering Fanged Skull (N)
        [67703]        = { ["item"] = {47303,47115}, ["icd"] = 45, ["spec"] = 0 },               -- Death's Choice, Death's Verdict (N-AGI)
        [67708]        = { ["item"] = {47303,47115}, ["icd"] = 45, ["spec"] = 0 },               -- Death's Choice, Death's Verdict (N-STR)
        [67772]        = { ["item"] = {47464,47131}, ["icd"] = 45, ["spec"] = 0 },               -- Death's Choice, Death's Verdict (HC-AGI)
        [67773]        = { ["item"] = {47464,47131}, ["icd"] = 45, ["spec"] = 0 },               -- Death's Choice, Death's Verdict (HC-STR)
        [71403]        = { ["item"] = {50198}, ["icd"] = 45, ["spec"] = 0 },                     -- Needle-Encrusted Scorpion
        [71610]        = { ["item"] = {50359}, ["icd"] = 45, ["spec"] = 0 },                     -- Althor's Abacus
        [71633]        = { ["item"] = {50352}, ["icd"] = 45, ["spec"] = 0 },                     -- Corpse-tongue coin
        [71584]        = { ["item"] = {50358}, ["icd"] = 45, ["spec"] = 0 },                     -- Purified Lunar Dust
        [71403]        = { ["item"] = {50198}, ["icd"] = 45, ["spec"] = 0 },                     -- Needle-Encrusted Scorpion
        [71610]        = { ["item"] = {50359}, ["icd"] = 45, ["spec"] = 0 },                     -- Althor's Abacus
        [71633]        = { ["item"] = {50352}, ["icd"] = 45, ["spec"] = 0 },                     -- Corpse-tongue coin
        [67750]        = { ["item"] = {47059,47432}, ["icd"] = 0, ["spec"] = 0 },                -- Solace of the Defeated (HC)
        [67696]        = { ["item"] = {47041,47271}, ["icd"] = 0, ["spec"] = 0 },                -- Solace of the Defeated (N)
        [72418]        = { ["item"] = {50400}, ["icd"] = 60, ["spec"] = 12 },                    -- Ashen Band of Endless Wisdom
        [72414]        = { ["item"] = {50404}, ["icd"] = 60, ["spec"] = 12 },                    -- Ashen Band of Endless Courage
        [72412]        = { ["item"] = {50402,52572}, ["icd"] = 60, ["spec"] = 12 },              -- Ashen Band of Endless Vengeance
        [72416]        = { ["item"] = {50398}, ["icd"] = 60, ["spec"] = 12 },                    -- Ashen Band of Endless Destruction
        [55775]        = { ["item"] = {55775}, ["icd"] = 60, ["spec"] = 15 },                    -- Swordguard Embroidery
        [55637]        = { ["item"] = {55637}, ["icd"] = 45, ["spec"] = 15 },                    -- Lightweave Embroidery
    }
end

function SimplyAlerts:ResetDefines(reset)
    local defaultsCopy = tableCopy(self.defaults.profile)

    if (reset == 1 or reset == 5) then -- general spell notif
        self.db.profile.controls = defaultsCopy.controls
        self.db.profile.chat = defaultsCopy.chat
    elseif (reset == 2 or reset == 5) then -- local aura notif
        self.db.profile.localmsgs = defaultsCopy.localmsgs
        self.db.profile.licon = defaultsCopy.licon
        self.db.profile.licsiz = defaultsCopy.licsiz
        self.db.profile.uppercase["local"] = defaultsCopy.uppercase["local"]
        self.db.profile.messages.lmsgds = defaultsCopy.messages.lmsgds
        self.db.profile.messages.lmsgsdw = defaultsCopy.messages.lmsgsdw
        self.db.profile.Spellz = defaultsCopy.Spellz
        self.db.profile.onlyarena["local"] = defaultsCopy.onlyarena["local"]
    elseif (reset == 3 or reset == 5) then -- group aura notif
        self.db.profile.chatmsgs = defaultsCopy.chatmsgs
        self.db.profile.uppercase.chat = defaultsCopy.uppercase.chat
        self.db.profile.messages.gmsgdw = defaultsCopy.messages.gmsgdw
        self.db.profile.messages.gmsgup = defaultsCopy.messages.gmsgup
        self.db.profile.Spellzp = defaultsCopy.Spellzp
        self.db.profile.onlyarena.chat = defaultsCopy.onlyarena.chat
    elseif (reset == 4 or reset == 5) then -- group spell status
        self.db.profile.spellstatus = defaultsCopy.spellstatus
        self.db.profile.onlyarena.spell = defaultsCopy.onlyarena.spell
    elseif (reset == 5) then 
        self.db.profile.markers = defaultsCopy.markers
    elseif (reset == 6 or reset == 5) then -- local alert
        self.db.profile.alert = defaultsCopy.alert
    elseif (reset == 7 or reset == 5) then -- local aura reminder
        self.db.profile.reminder = defaultsCopy.reminder
        self.db.profile.onlyarena.reminder = defaultsCopy.onlyarena.reminder
    elseif (reset == 8 or reset == 5) then -- spell reporter
        self.db.profile.spellstatus.reporter = defaultsCopy.spellstatus.reporter
    elseif (reset == 9 or reset == 5) then -- icd bars
        self.db.profile.bars = defaultsCopy.bars
        for h,_ in pairs(self.db.profile.bars.barst) do
            if (self.db.profile.bars.barst[h].enabled) then
                self:UpdateICDBar(h)
            end
        end
    elseif (reset == 10 or reset == 5) then -- arena dodger
        self.db.profile.dodger = defaultsCopy.dodger
    elseif (reset == 11 or reset == 5) then -- arena dodger
        self.db.profile.markers = defaultsCopy.markers
    elseif (reset == 12 or reset == 5) then -- frames colored by class
        self.db.profile.classframes = defaultsCopy.classframes
    elseif (reset == 13 or reset == 5) then -- improved power bars
        self.db.profile.statusBars = defaultsCopy.statusBars
    elseif (reset == 14 or reset == 5) then -- auto cancel soul fragments
        self.db.profile.autoCancelSoulFragment = defaultsCopy.autoCancelSoulFragment
    end
end