local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts")
local L, SM = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true), LibStub("LibSharedMedia-3.0")
local self = SimplyAlerts
local icdIndex, markerIndex, powerBarsIndex, classColorIndex = "player", "player", "player", "health"

local function DeleteNotificSpell(info, value, table)
    for k,v in ipairs(table) do
        if (v == value) then
            DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts:|r " .. SimplyAlerts:SpellName(value) .. "(" .. value .. ") successfully deleted")
            return true
        end
    end
    return false
end

local function AddNotificSpell(info, value, table)
    if (string.len(value) > 2 and string.match(value, "%d")) then
        for _,v in ipairs(table) do
            if (v == tonumber(value)) then
                DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts:|r Inserted value is already in the list!")
                return false
            end
        end
    else
        DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts:|r Inserted value isn't a valid SPELLID!")
        return false
    end

    DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts:|r " .. SimplyAlerts:SpellName(value) .. "(" .. value .. ") successfully added")
    return true
end

local function firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

local function barsUpdate()
    for k,_ in pairs(self.db.profile.bars.barst) do
        if (self.db.profile.bars.barst[k].enabled) then
            self:UpdateICDBar(k)
        end
    end
end

local function varUpdate(unit, var, val)
    if (self.db.profile.bars["global"]) then
        for k,_ in pairs(self.db.profile.bars.barst) do
            self.db.profile.bars.barst[k][var] = val
            if (self.db.profile.bars.barst[k].enabled) then
                self:UpdateICDBar(k)
            end
        end
    else
        self.db.profile.bars.barst[unit][var] = val
        barsUpdate()
    end
end

function SimplyAlerts:GetOptions()
    self.options = {
        name = "SimplyAlerts",
        handler = SimplyAlerts,
        type = "group",
        args = {
            Informations = {
                 type = "group",
                 name = L["Informations"],
                 desc = L["Credits, author, reset.."],
                 order = 3,
                 args = {
                    about = {
                        type = "description",
                        name = L["Lang_Info"],
                        order = 1
                        
                    },
                    ResetAddon = {
                        type = "execute",
                        name = "Reset Addon",
                        desc = "This will reset all addon settings to default!",
                        func = function() self:ResetDefines(5) end,
                        order = 2
                    },
                },
            },
            Extra = {
                type = "group",
                name = L["Extra"],
                desc = L["Special functions"],
                order = 2,
                args = {
                    Markers = {
                        name = L["Raid Markers"],
                        desc = L["Special functions"],
                        type = "group",
                        order = 1,
                        args = {
                            autoMarker = {
                                type = "toggle",
                                name = L["Auto raid target icons"],
                                desc = L["Automatic raid marks before arena starts"],
                                set = function (info, value) self.db.profile.markers.marker = value end,
                                get = function (info) return self.db.profile.markers.marker end,
                                order = 1,
                            },
                            petMarker = {
                                type = "toggle",
                                name = L["Auto pet remarker"],
                                desc = L["Automatically remark pets after they died and were revived"],
                                set = function (info, value) self.db.profile.markers.petmark = value end,
                                get = function (info) return self.db.profile.markers.petmark end,
                                order = 2,
                            },
                            markerUnit = {
                                type = "select",
                                name = L["Select unit to set party mark"],
                                get = function (info) return markerIndex end,
                                set = function (info, value) markerIndex = value end,
                                values = function (info) local tab = { }
                                    for k,_ in pairs(self.db.profile.markers.units) do
                                        tab[k] = firstToUpper(string.lower(k))
                                    end
                                    return tab
                                end,
                                order=3,
                            },
                            setMark = {
                                type = "select",
                                name = L["Select mark for selected unit"],
                                get = function (info) return self.db.profile.markers.units[markerIndex] end,
                                set = function (info, value) self.db.profile.markers.units[markerIndex] = value end,
                                values = function (info)
                                    local tab = { [0]="None" }
                                    local iconNames = {
                                        "Star",
                                        "Circle",
                                        "Diamond",
                                        "Triangle",
                                        "Moon",
                                        "Square",
                                        "Cross",
                                        "Skull"
                                    }

                                    for index, iconPath in pairs(ICON_LIST) do
                                        tab[index] = iconPath .. "0|t " .. iconNames[index]
                                    end
                                    
                                    -- tag already used ones
                                    for _, iconId in pairs(self.db.profile.markers.units) do
                                        if iconId > 0 and string.sub(tab[iconId], -1) ~= "]" then
                                            tab[iconId] = tab[iconId] .. " [ASSIGNED]"
                                        end
                                    end

                                    return tab
                                end,
                                order=4,
                            },
                            ResetMarkers = {
                                type = "execute",
                                name = L["Reset Markers"],
                                desc = L["This will reset all Markers settings!"],
                                func = function() self:ResetDefines(11) end,
                                order = 10,
                            },
                        }
                    },
                    classFrames = {
                        type = "group",
                        name = L["Class color frames"],
                        desc = L["Colored health bars and name background with class color of unit"],
                        order = 2,
                        args = {
                            toggleClassFrames = {
                                type = "toggle",
                                name = L["Enable coloring"],
                                set = function (info, value) 
                                        self.db.profile.classframes.enabled = value
                                        self:UpdateClassFrame(PlayerFrame)
                                    end,
                                get = function (info) return self.db.profile.classframes.enabled end,
                                order = 1,
                            },
                            selectColoringType = {
                                type = "select",
                                name = L["Coloring mode"],
                                desc = L["Select a mode in which the frames will be colored"],
                                get = function (info) return self.db.profile.classframes.mode end,
                                set = function (info, value) 
                                        self.db.profile.classframes.mode = value
                                        self:UpdateClassFrame(PlayerFrame)
                                        self:UpdateClassFrame(TargetFrame)
                                        self:UpdateClassFrame(TargetFrameToT)
                                        self:UpdateClassFrame(FocusFrame)
                                    end,
                                values = {
                                    all = "BOTH",
                                    health = "Only HEALTH bar",
                                    name = "Only NAME background",
                                },
                                order=2,
                            },
                            translucencyMode = {
                                type = "select",
                                name = L["Modify a field"],
                                desc = L["Select a field to modify its behaviour"],
                                get = function (info) return classColorIndex end,
                                set = function (info, value) classColorIndex = value end,
                                values = {
                                    health = "Health Bar",
                                    name = "Name Background",
                                },
                                order=2,
                            },
                            disableColoring = {
                                type = "select",
                                name = L["Disable Coloring"],
                                desc = L["Disable coloring of currently selected field in frames"],
                                get = function (info) 
                                        if self.db.profile.classframes.translucency[classColorIndex].player.disabled then
                                            return "player"
                                        elseif self.db.profile.classframes.translucency[classColorIndex].other.disabled then
                                            return "other"
                                        else
                                            return "none"
                                        end
                                    end,
                                set = function (info, value)
                                        self.db.profile.classframes.translucency[classColorIndex].player.disabled = false
                                        self.db.profile.classframes.translucency[classColorIndex].other.disabled = false

                                        if value ~= "none" then
                                            self.db.profile.classframes.translucency[classColorIndex][value].disabled = true
                                        end

                                        self:UpdateClassFrame(PlayerFrame)
                                        self:UpdateClassFrame(TargetFrame)
                                        self:UpdateClassFrame(TargetFrameToT)
                                        self:UpdateClassFrame(FocusFrame)
                                    end,
                                values = {
                                    none = "NONE",
                                    player = "Player Frames",
                                    other = "In all other frames",
                                },
                                order=3,
                            },
                            translucencyPlayer = {
                                type = "range",
                                name = L["Translucency PLAYER"],
                                desc = L["Set translucency for ONLY PLAYER frames"],
                                set = function (info, value)
                                        self.db.profile.classframes.translucency[classColorIndex].player.val = value
                                        self:UpdateClassFrame(PlayerFrame)
                                    end,
                                get = function (info) return self.db.profile.classframes.translucency[classColorIndex].player.val end,
                                min = 0.1,
                                max = 1,
                                step = 0.01,
                                order = 4,
                            },
                            translucencyOther = {
                                type = "range",
                                name = L["Translucency OTHER"],
                                desc = L["Set translucency for all OTHER frames except PLAYER FRAMES"],
                                set = function (info, value)
                                        self.db.profile.classframes.translucency[classColorIndex].other.val = value
                                        self:UpdateClassFrame(TargetFrame)
                                        self:UpdateClassFrame(TargetFrameToT)
                                        self:UpdateClassFrame(FocusFrame)
                                    end,
                                get = function (info) return self.db.profile.classframes.translucency[classColorIndex].other.val end,
                                min = 0.1,
                                max = 1,
                                step = 0.01,
                                order = 5,
                            },
                            ResetColoredFrames = {
                                type = "execute",
                                name = L["Reset Colored Frames"],
                                desc = L["This will reset all settings for Class Colored Frames!"],
                                func = function() 
                                        self:ResetDefines(12)
                                        self:UpdateClassFrame(PlayerFrame)
                                        self:UpdateClassFrame(TargetFrame)
                                        self:UpdateClassFrame(TargetFrameToT)
                                        self:UpdateClassFrame(FocusFrame)
                                    end,
                                order = 6,
                            },
                        }
                    }, 
                    powerBars = {
                        type = "group",
                        name = L["Health and Power bars"],
                        desc = L["Adds percentages to the left side of haelth and power bars"],
                        order = 3,
                        args = {
                            togglePercentages = {
                                type = "toggle",
                                name = L["Enable percentages"],
                                set = function (info, value) 
                                        self.db.profile.statusBars.enabled = value
                                        self:UpdateStatusBar(PlayerFrameHealthBar)
                                        self:UpdateStatusBar(PlayerFrameManaBar)
                                        self:UpdateStatusBar(PartyMemberFrame1HealthBar)
                                        self:UpdateStatusBar(PartyMemberFrame1ManaBar)
                                    end,
                                get = function (info) return self.db.profile.statusBars.enabled end,
                                order = 1,
                            },
                            setSpecificFrame = {
                                type = "select",
                                name = L["Specific frame"],
                                desc = L["Update values for specific frames"],
                                get = function (info) return powerBarsIndex end,
                                set = function (info, value) powerBarsIndex = value end,
                                values = {
                                    player = "Player",
                                    pet = "Pet",
                                    party = "Party",
                                    target = "Target"
                                },
                                order=2,
                            },
                            toggleSpecificFrame = {
                                type = "toggle",
                                name = L["Enable for specific frame"],
                                desc = L["Enable percentages for specific frame"],
                                set = function (info, value) 
                                        self.db.profile.statusBars.barTypes[powerBarsIndex].enabled = value
                                        self:UpdateStatusBar(PlayerFrameHealthBar)
                                        self:UpdateStatusBar(PlayerFrameManaBar)
                                    end,
                                get = function (info) return self.db.profile.statusBars.barTypes[powerBarsIndex].enabled end,
                                order = 3,
                            },
                            togglePowerTypes = {
                                type = "select",
                                name = L["Enable only for specific power types"],
                                desc = L["Select option for which power type percentages will be shown"],
                                set = function (info, value)
                                        self.db.profile.statusBars.barTypes[powerBarsIndex].powers[value] = not self.db.profile.statusBars.barTypes[powerBarsIndex].powers[value]
                                        self:Updated(self:GetPowerName(value),self.db.profile.statusBars.barTypes[powerBarsIndex].powers[value],false)

                                        if powerBarsIndex == "player" then
                                            self:UpdateStatusBar(PlayerFrameHealthBar)
                                            self:UpdateStatusBar(PlayerFrameManaBar)
                                        end
                                    end,
                                values = function (info)
                                    local tab = {
                                        [6] = "[" .. string.upper(tostring(self.db.profile.statusBars.barTypes[powerBarsIndex].powers[6])) .. "] " .. self:GetPowerName(6),
                                        [3] = "[" .. string.upper(tostring(self.db.profile.statusBars.barTypes[powerBarsIndex].powers[3])) .. "] " .. self:GetPowerName(3),
                                        [2] = "[" .. string.upper(tostring(self.db.profile.statusBars.barTypes[powerBarsIndex].powers[2])) .. "] " .. self:GetPowerName(2),
                                        [1] = "[" .. string.upper(tostring(self.db.profile.statusBars.barTypes[powerBarsIndex].powers[1])) .. "] " .. self:GetPowerName(1),
                                        [0] = "[" .. string.upper(tostring(self.db.profile.statusBars.barTypes[powerBarsIndex].powers[0])) .. "] " .. self:GetPowerName(0)
                                    }
                                    
                                    return tab
                                end,
                                order=4,
                            },
                            percOffset = {
                                type = "range",
                                name = L["Percentages offset"],
                                desc = L["Set offset for percentage value"] .. L["RELOAD_UI"],
                                set = function (info, value)
                                        self.db.profile.statusBars.barTypes[powerBarsIndex].offPerc = value
                                        self:UpdateStatusBarPoints(PlayerFrameHealthBar)
                                        self:UpdateStatusBarPoints(PlayerFrameManaBar)
                                        self:UpdateStatusBarPoints(PetFrameHealthBar)
                                        self:UpdateStatusBarPoints(PetFrameManaBar)
                                        self:UpdateStatusBarPoints(TargetFrameHealthBar)
                                        self:UpdateStatusBarPoints(TargetFrameManaBar)
                                        self:UpdateStatusBarPoints(PartyMemberFrame1HealthBar)
                                        self:UpdateStatusBarPoints(PartyMemberFrame1ManaBar)
                                    end,
                                get = function (info) return self.db.profile.statusBars.barTypes[powerBarsIndex].offPerc end,
                                min = 20,
                                max = 70,
                                step = 1,
                                order = 5,
                            },
                            powerOffset = {
                                type = "range",
                                name = L["Amount offset"],
                                desc = L["Set offset for amount of current power (mana, rage)"] .. L["RELOAD_UI"],
                                set = function (info, value)
                                        self.db.profile.statusBars.barTypes[powerBarsIndex].offPower = value
                                        self:UpdateStatusBarPoints(PlayerFrameHealthBar)
                                        self:UpdateStatusBarPoints(PlayerFrameManaBar)
                                        self:UpdateStatusBarPoints(PetFrameHealthBar)
                                        self:UpdateStatusBarPoints(PetFrameManaBar)
                                        self:UpdateStatusBarPoints(TargetFrameHealthBar)
                                        self:UpdateStatusBarPoints(TargetFrameManaBar)
                                        self:UpdateStatusBarPoints(PartyMemberFrame1HealthBar)
                                        self:UpdateStatusBarPoints(PartyMemberFrame1ManaBar)
                                    end,
                                get = function (info) return self.db.profile.statusBars.barTypes[powerBarsIndex].offPower end,
                                min = 20,
                                max = 70,
                                step = 1,
                                order = 6,
                            },
                            updateDisplay = {
                                type = "toggle",
                                name = L["Replace max value"],
                                desc = L["Replace max value with only current value for disabled power types (e.g: 20 / 100 -> 20)"],
                                set = function (info, value) 
                                        self.db.profile.statusBars.updateDisplay = value
                                        self:UpdateClassFrame(PlayerFrame)
                                    end,
                                get = function (info) return self.db.profile.statusBars.updateDisplay end,
                                order = 7,
                            },
                            ResetAlert = {
                                type = "execute",
                                name = L["Reset Settings"],
                                desc = L["This will reset all \"Health and Power bars\" settings"],
                                func = function() self:ResetDefines(13) end,
                                order = 8,
                            },
                        }
                    },
                    cancelSoulFragments = {
                        name = L["Cancel Soul Fragments"],
                        desc = L["Automatically cancel Soul Fragments at 9 stacks"],
                        type = "group",
                        order = 4,
                        args = {
                            cancelSoulFragmentsToggle = {
                                type = "toggle",
                                name = L["Cancel Soul Fragments"],
                                desc = L["Automatically cancel Soul Fragments at 9 stacks"],
                                set = function (info, value) self.db.profile.autoCancelSoulFragment.enabled = value end,
                                get = function (info) return self.db.profile.autoCancelSoulFragment.enabled end,
                                order = 1,
                            },
                            notifyMe = {
                                type = "toggle",
                                name = L["Notify me"],
                                desc = L["Send me local message when Soul Fragments are canceled"],
                                set = function (info, value) self.db.profile.autoCancelSoulFragment.notify = value end,
                                get = function (info) return self.db.profile.autoCancelSoulFragment.notify end,
                                order = 2,
                            },
                            resetCancelSoulFragments = {
                                type = "execute",
                                name = L["Reset Cancel Soul Fragments"],
                                desc = L["This will reset all Cancel Soul Fragments settings!"],
                                func = function() self:ResetDefines(14) end,
                                order = 3,
                            },
                        }
                    },
                    -- AutoRBG = { -- Disabled due to restriction of WoW Lua
                    --     type = "toggle",
                    --     name = L["Auto random battlegrounds (requires LuaUnlocker!)"],
                    --     desc = L["Automatic random battlegrounds"],
                    --     set = function (info, value) self.db.profile.autorbg = value; self:JoinBattleground(); end,
                    --     get = function (info) return self.db.profile.autorbg end,
                    --     order = 3,
                    -- },
                    -- Dodger = { -- Disabled due to restriction of WoW Lua
                    --     type = "group",
                    --     name = L["Arena Dodger"],
                    --     desc = L["Do you want to not lose rating? Dodge them"],
                    --     order = 1,
                    --     args = {
                    --         enableDodger = {
                    --             type = "toggle",
                    --             name = L["Arena Dodger"],
                    --             desc = L["Enable automatic queue leave when selected friends are out of the arena"],
                    --             set = function (info, value) self.db.profile.dodger.enabled = value end,
                    --             get = function (info) return self.db.profile.dodger.enabled end,
                    --             order = 1,
                    --         },
                    --         ignoreAFK = {
                    --             type = "toggle",
                    --             name = L["Ignore AFK"],
                    --             desc = L["Ignore players with /away status"],
                    --             set = function (info, value) self.db.profile.dodger.ignoreafk = value end,
                    --             get = function (info) return self.db.profile.dodger.ignoreafk end,
                    --             order = 2,
                    --         },
                    --         enableNote = {
                    --             type = "toggle",
                    --             name = L["Friendlist Note"],
                    --             desc = L["Enable dodging only by specific note"],
                    --             set = function (info, value) self.db.profile.dodger.note.enabled = value end,
                    --             get = function (info) return self.db.profile.dodger.note.enabled end,
                    --             order = 3,
                    --         },
                    --         noteString = {
                    --             type = "input",
                    --             name = L["Note String"],
                    --             desc = L["Insert specific friendlist note string to be dodged"],
                    --             get = function (info) return self.db.profile.dodger.note.string end,
                    --             set = function (info, value) self.db.profile.dodger.note.string = value end,
                    --             order = 4,
                    --         },
                    --         resetDodger = {
                    --             type = "execute",
                    --             name = L["Reset Arena Dodger"],
                    --             desc = L["This will reset all Arena Dodger settings!"],
                    --             func = function() self:ResetDefines(10) end,
                    --             order = 10,
                    --         },
                    --     },
                    -- },
                },
            },

            Settings = {
                type = "group",
                name = L["Spell Notifications"],
                desc = L["Configure notifications"],
                order = 1,
                args = {
                    controlMsgs = {
                        type = "toggle",
                        name = L["Toggle CC Notifications"],
                        desc = L["Messages about CC (like 'SEDUCED') to group members"],
                        set = function (info, value) self.db.profile.controls = value end,
                        get = function (info) return self.db.profile.controls end,
                        order = 1,
                    },
                    messageType = {
                        type = "select",
                        name = L["Message type"],
                        desc = L["Where to display ALL (group) announcement messages"],
                        get = function (info) return self.db.profile.chat end,
                        set = function (info, value) self.db.profile.chat = value end,
                        values = {
                            party = "Party",
                            raid = "Raid",
                            raid_warning = "Raid Warning",
                            battleground = "Battleground",
                        },
                        order=2,
                    },
                    ResetSpellNot = {
                        type = "execute",
                        name = L["Defaults"],
                        func = function() self:ResetDefines(1) end,
                        order = 3,
                    },

                    LocalMsgs = {
                        type="group",
                        name=L["Aura Notifications (Local)"],
                        desc=L["Aura messages (like spell dispells) to player (you)"],
                        order=2,
                        args = {
                            Alert = {
                                type="group",
                                name=L["Alert Settings"],
                                order=1,
                                args = {
                                    font = {
                                        type = "select",
                                        name = L["Font"],
                                        set = function (info, value) self.db.profile.alert["font"] = value; self:UpdateFrame() end,
                                        get = function (info) return self.db.profile.alert["font"] end,
                                        values = function()
                                                self.fonts = self.fonts or {}
                                                wipe(self.fonts)
                                                for _, name in pairs(SM:List("font")) do 
                                                    self.fonts[name] = name
                                                end
                                                return self.fonts
                                            end,
                                        order = 0,
                                    },
                                    fontsize = {
                                        type = "range",
                                        name = L["Fontsize"],
                                        set = function (info, value) self.db.profile.alert["fontsize"] = value; self:UpdateFrame() end,
                                        get = function (info) return self.db.profile.alert["fontsize"] end,
                                        min = 10,
                                        max = 40,
                                        step = 1,
                                        order = 1,
                                    },
                                    holdTime = {
                                        type = "range",
                                        name = L["Hold Time"],
                                        set = function (info, value) self.db.profile.alert["holdtime"] = value end,
                                        get = function (info) return self.db.profile.alert["holdtime"] end,
                                        order = 2,
                                        min = 0.5,
                                        max = 5,
                                        step = 0.1,
                                    },
                                    lock = {
                                        type = "toggle",
                                        name = L["Lock"],
                                        set = function (info, value) self.db.profile.alert["lock"] = value; self:UpdateFrame() end,
                                        get = function (info) return self.db.profile.alert["lock"] end,
                                        order = 3,
                                    },
                                    ResetAlert = {
                                        type = "execute",
                                        name = L["Reset Alert Settings"],
                                        desc = L["This will reset all Alert settings including position!"],
                                        func = function() self:ResetDefines(6) end,
                                        order = 4,
                                    },
                                },
                            },
                            localMsgs = {
                                type = "toggle",
                                name = L["Toggle Aura Notifications"],
                                set = function (info, value) self.db.profile.localmsgs = value end,
                                get = function (info) return self.db.profile.localmsgs end,
                                order = 1,
                            },
                            iconToggle = {
                                type = "toggle",
                                name = L["Toggle Spell Icon"],
                                desc = L["Spell icon in aura spell notification"],
                                set = function (info, value) self.db.profile.licon = value end,
                                get = function (info) return self.db.profile.licon end,
                                order = 2,
                            },
                            iconSize = {
                                type = "range",
                                name = L["Spell icon size"],
                                desc = L["Set size of spell icon"],
                                set = function (info, value) self.db.profile.licsiz = value end,
                                get = function (info) return self.db.profile.licsiz end,
                                min = 1,
                                max = 20,
                                step = 1,
                                order = 3,
                            },
                            upperCasel = {
                                type = "select",
                                name = L["Select Uppercase"],
                                desc = L["Select where you want to use uppercases for spell name"],
                                get = function (info) return self.db.profile.uppercase["local"] end,
                                set = function (info, value) self.db.profile.uppercase["local"] = value end,
                                values = {
                                    bothd = L["Both Disabled"],
                                    bothe = L["Both Enabled"],
                                    dsonly = L["Only 'Dispells'"],
                                    dwonly = L["Only 'Down'"],
                                },
                                order = 4,
                            },
                            dpMessagel = {
                                type = "input",
                                name = L["Set 'Dispell' Message"],
                                desc = L["Set 'dispell' message, use '%s' for spell name"],
                                get = function (info) return self.db.profile.messages["lmsgsds"] end,
                                set = function (info, value) self.db.profile.messages["lmsgsds"] = value end,
                                order = 5,
                            },
                            dwMessagel = {
                                type = "input",
                                name = L["Set 'Down' Message"],
                                desc = L["Set 'down' message, use '%s' for spell name"],
                                get = function (info) return self.db.profile.messages["lmsgsdw"] end,
                                set = function (info, value) self.db.profile.messages["lmsgsdw"] = value end,
                                order = 6,
                            },
                            deleteSpells = {
                                type = "select",
                                name = L["Delete announced spells"],
                                desc = L["Select spell which will be deleted from announced spells list"],
                                set = function (info, value) 
                                        if DeleteNotificSpell(info, value, self.db.profile.Spellz) then 
                                            table.remove(self.db.profile.Spellz, value)
                                        end
                                    end,
                                values = function (info) local tab = { }
                                        for _, spellId in ipairs(self.db.profile.Spellz) do
                                            local name = GetSpellInfo(spellId)
                                            tab[spellId] = "[" .. spellId .. "] " .. name
                                        end
                                        return tab
                                    end,
                                order = 7,
                            },
                            addSpells = {
                                type = "input",
                                name = L["Add spells for announcements"],
                                desc = L["Insert SpellID to add spell to announcement spell list"],
                                set = function (info, value) 
                                        if (AddNotificSpell(info, value, self.db.profile.Spellz)) then 
                                            table.insert(self.db.profile.Spellz, tonumber(value))
                                        end
                                    end,
                                order = 8,
                            },
                            loclArena = {
                                type = "toggle",
                                name = L["Arena Only"],
                                desc = L["Enable Local Notifications only in arena"],
                                set = function (info, value) self.db.profile.onlyarena["local"] = value end,
                                get = function (info) return self.db.profile.onlyarena["local"] end,
                                order = 9,
                            },
                            ResetLocalNot = {
                                type = "execute",
                                name = L["Reset Local Notifications"],
                                desc = L["This will reset all Local Notifications settings!"],
                                func = function() self:ResetDefines(2) end,
                                order = 10,
                            },
                        },
                    },

                    ChatMsgs = {
                        type="group",
                        name = L["Aura Notifications (Group)"],
                        desc = L["Aura messages (like spell dispells) to group members"],
                        order=3,
                        args = {
                            chatMsgs = {
                                type = "toggle",
                                name = L["Toggle Aura Notifications"],
                                desc = L["Aura messages (like spell dispells) to group members"],
                                set = function (info, value) self.db.profile.chatmsgs = value end,
                                get = function (info) return self.db.profile.chatmsgs end,
                                order = 1,
                            },
                            upperCaseg = {
                                type = "select",
                                name = L["Select Uppercase"],
                                desc = L["Select where you want to use uppercases for spell name"],
                                get = function (info) return self.db.profile.uppercase["chat"] end,
                                set = function (info, value) self.db.profile.uppercase["chat"] = value end,
                                values = {
                                    bothd = L["Both Disabled"],
                                    bothe = L["Both Enabled"],
                                    dwonly = L["Only 'Down'"],
                                    uponly = L["Only 'Up'"],
                                },
                                order = 2,
                            },
                            dwMessageg = {
                                type = "input",
                                name = L["Set 'Down' Message"],
                                desc = L["Set 'down' message, use '%s' for spell name"],
                                get = function (info) return self.db.profile.messages["gmsgdw"] end,
                                set = function (info, value) self.db.profile.messages["gmsgdw"] = value end,
                                order = 3,
                            },
                            upMessageg = {
                                type = "input",
                                name = L["Set 'Up' Message"],
                                desc = L["Set 'up' message, use '%s' for spell name"],
                                get = function (info) return self.db.profile.messages["gmsgup"] end,
                                set = function (info, value) self.db.profile.messages["gmsgup"] = value end,
                                order = 4,
                            },
                            deleteSpellsp = {
                                type = "select",
                                name = L["Delete announced spells"],
                                desc = L["Select spell which will be deleted from announced spells list"],
                                set = function (info, value) 
                                        if DeleteNotificSpell(info, value, self.db.profile.Spellzp) then 
                                            table.remove(self.db.profile.Spellzp, value)
                                        end
                                    end,
                                values = function (info) local tab = { }
                                        for _, spellId in ipairs(self.db.profile.Spellzp) do
                                            local spellName = GetSpellInfo(spellId)
                                            tab[spellId] = "[" .. spellId .. "] " .. spellName
                                        end
                                        return tab
                                    end,
                                order=5,
                            },
                            addSpellsp = {
                                type = "input",
                                name = L["Add spells for announcements"],
                                desc = L["Insert SpellID to add spell to announcement spell list"],
                                set = function (info, value) 
                                        if (AddNotificSpell(info, value, self.db.profile.Spellzp)) then 
                                            table.insert(self.db.profile.Spellzp, tonumber(value))
                                        end
                                    end,
                                order = 6,
                            },
                            chatArena = {
                                type = "toggle",
                                name = L["Arena Only"],
                                desc = L["Enable Chat Notifications only in arena"],
                                set = function (info, value) self.db.profile.onlyarena["chat"] = value end,
                                get = function (info) return self.db.profile.onlyarena["chat"] end,
                                order = 7,
                            },
                            ResetGroupNot = {
                                type = "execute",
                                name = L["Reset Group Notifications"],
                                desc = L["This will reset all Group Notifications settings!"],
                                func = function() self:ResetDefines(3) end,
                                order = 8,
                            },
                        },
                    },

                    SpellStatus = {
                        type="group",
                        name=L["Spell Status (Group)"],
                        desc=L["Spell messages about misses/dodges, etc.."],
                        order=4,
                        args = {
                            statusToggle = {
                                type = "toggle",
                                name = L["Spell Status"],
                                desc = L["Spell messages about misses/dodges, etc.."],
                                get = function (info) return self.db.profile.spellstatus.enabled end,
                                set = function (info, value) self.db.profile.spellstatus.enabled = value end,
                                order = 1,
                            },
                            messageType = {
                                type = "select",
                                name = L["Filtered Groups"],
                                desc = L["Select which group of players will be checked"],
                                get = function (info) return self.db.profile.spellstatus["type"] end,
                                set = function (info, value) self.db.profile.spellstatus["type"] = value end,
                                values = {
                                    me = L["Only Me"],
                                    meparty = L["Me + Party members"],
                                    party = L["Only Party members"],
                                    all = L["All"],
                                },
                                order=2,
                            },
                            spellArena = {
                                type = "toggle",
                                name = L["Arena Only"],
                                desc = L["Enable Spell Status alerts only in arena"],
                                set = function (info, value) self.db.profile.onlyarena["spell"] = value end,
                                get = function (info) return self.db.profile.onlyarena["spell"] end,
                                order = 3,
                            },
                            missTypes = {
                                type = "select",
                                name = L["Enable/Disable spell miss type"],
                                desc = L["Select miss type which will have enabled/disabled announce"],
                                set = function (info, value)
                                        self.db.profile.spellstatus.filter[value] = not self.db.profile.spellstatus.filter[value]
                                        self:Updated(value,self.db.profile.spellstatus.filter[value],false)
                                    end,
                                values = function (info) local tab = { }
                                        for missType, boolValue in pairs(self.db.profile.spellstatus.filter) do
                                            tab[missType] = "[" .. string.upper(tostring(boolValue)) .. "] " .. firstToUpper(string.lower(missType))
                                        end
                                        return tab
                                    end,
                                order=4,
                            },
                            ResetSpellStatus = {
                                type = "execute",
                                name = L["Reset Spell Status"],
                                desc = L["This will reset all Spell Status settings!"],
                                func = function() self:ResetDefines(4) end,
                                order = 5,
                            },

                            SpellAlerter = {
                                type="group",
                                name=L["Spell Alerter"],
                                desc=L["Messages about how much misses/dodges.. you did (Arena Only)"],
                                order=1,
                                args = {
                                    sAlerterTogg = {
                                        type = "toggle",
                                        name = L["Toggle Spell Alerter"],
                                        set = function (info, value) self.db.profile.spellstatus.reporter.enabled = value end,
                                        get = function (info) return self.db.profile.spellstatus.reporter.enabled end,
                                        order = 0,
                                    },
                                    sAlerterType = {
                                        type = "select",
                                        name = L["Types"],
                                        desc = L["Select which miss types will be counted"],
                                        set = function (info, value)
                                                self.db.profile.spellstatus.reporter.types[value] = not self.db.profile.spellstatus.reporter.types[value]
                                                self:Updated(value,self.db.profile.spellstatus.reporter.types[value],false)
                                            end,
                                        values = function (info) local tab = { }
                                                for who, boolValue in pairs(self.db.profile.spellstatus.reporter.types) do
                                                    tab[who] = "[" .. string.upper(tostring(boolValue)) .. "] By " .. firstToUpper(string.lower(who))
                                                end
                                                return tab
                                            end,
                                        order=1,
                                    },
                                    sAlerterChat = {
                                        type = "select",
                                        name = L["Chat Type"],
                                        desc = L["Select where report should be reported"],
                                        get = function (info) return self.db.profile.spellstatus.reporter["chat"] end,
                                        set = function (info, value) self.db.profile.spellstatus.reporter["chat"] = value end,
                                        values = {
                                            me = "Self",
                                            party = "Party",
                                            raid = "Raid",
                                            battleground = "Battleground",
                                        },
                                        order=2,
                                    },
                                    ResetSpellAlerter = {
                                        type = "execute",
                                        name = L["Reset Spell Alerter"],
                                        desc = L["This will reset all Spell Alerter settings!"],
                                        func = function() self:ResetDefines(8) end,
                                        order = 3,
                                    }
                                },
                            },
                        },
                    },

                    SpellReminder = {
                        type="group",
                        name=L["Spell Reminder (Local)"],
                        desc=L["Remind buffs that faded / were dispelled.."],
                        order=5,
                        args = {
                            reminderToggle = {
                                type = "toggle",
                                name = L["Spell Reminder"],
                                desc = L["Remind buffs that faded / were dispelled.."],
                                get = function (info) return self.db.profile.reminder.enabled end,
                                set = function (info, value) self.db.profile.reminder.enabled = value end,
                                order = 1,
                            },
                            holdTime = {
                                type = "range",
                                name = L["Interval"],
                                desc = L["Interval between alerts"],
                                set = function (info, value) self.db.profile.reminder["interval"] = value end,
                                get = function (info) return self.db.profile.reminder["interval"] end,
                                order = 2,
                                min = 20,
                                max = 60,
                                step = 2,
                            },
                            deleteSpells = {
                                type = "select",
                                name = L["Delete reminder spells"],
                                desc = L["Select spell which will be deleted from reminder spells list"],
                                set = function (info, value)
                                        if DeleteNotificSpell(info, value, self.db.profile.reminder["spells"]) then
                                            table.remove(self.db.profile.reminder["spells"], value)
                                        end
                                    end,
                                values = function (info) local tab = { }
                                        for _, spellId in ipairs(self.db.profile.reminder["spells"]) do
                                            local spellName = GetSpellInfo(spellId)
                                            tab[spellId] = "[" .. spellId .. "] " .. spellName
                                        end
                                        return tab
                                    end,
                                order = 3,
                            },
                            addSpells = {
                                type = "input",
                                name = L["Add spells to reminder"],
                                desc = L["Insert SpellID to add spell to reminder spell list"],
                                set = function (info, value)
                                        if (AddNotificSpell(info, value, self.db.profile.reminder["spells"])) then
                                            table.insert(self.db.profile.reminder["spells"], tonumber(value))
                                        end
                                    end,
                                order = 4,
                            },
                            reminderArena = {
                                type = "toggle",
                                name = L["Arena Only"],
                                desc = L["Enable Spell Reminder alerts only in arena"],
                                set = function (info, value) self.db.profile.onlyarena["reminder"] = value end,
                                get = function (info) return self.db.profile.onlyarena["reminder"] end,
                                order = 5,
                            },
                            ResetSpellReminder = {
                                type = "execute",
                                name = L["Reset Spell Reminder"],
                                desc = L["This will reset all Spell Reminder settings!"],
                                func = function() self:ResetDefines(7) end,
                                order = 6,
                            },
                        },
                    },
                    ICDBars = {
                        type="group",
                        name=L["ICD Bar (Local)"],
                        desc=L["Internal Cooldowns Bar"],
                        order=6,
                        args = {
                            barsToggle = {
                                type = "toggle",
                                name = L["Lock Bars"],
                                desc = L["Lock activated bars"],
                                get = function (info) return self.db.profile.bars["lock"] end,
                                set = function (info, value) self.db.profile.bars["lock"] = value; barsUpdate() end,
                                order = 0,
                            },
                            globalToggle = {
                                type = "toggle",
                                name = L["Global Settings"],
                                desc = L["Every single change will be set to every bar types (except toggling the bars & cd text)"],
                                get = function (info) return self.db.profile.bars["global"] end,
                                set = function (info, value) self.db.profile.bars["global"] = value; end,
                                order = 1,
                            },
                            cdtextToggle = {
                                type = "toggle",
                                name = L["Enable CD Text"],
                                desc = L["Enable/Disable text of cooldowns"],
                                get = function (info) return self.db.profile.bars["showtext"] end,
                                set = function (info, value) self.db.profile.bars["showtext"] = value; end,
                                order = 2,
                            },
                            barSelect = {
                                type = "select",
                                name = L["Select bar type to edit"],
                                set = function (info, value) icdIndex = value end,
                                get = function (info) return icdIndex end,
                                values = function (info) local tab = { }
                                        for unit,_ in pairs(self.db.profile.bars.barst) do
                                            tab[unit] = "[" .. firstToUpper(string.lower(unit)) .. "] " .. string.upper(tostring(self.db.profile.bars.barst[unit].enabled))
                                        end
                                        return tab
                                    end,
                                order=3,
                            },
                            barToggle = {
                                type = "toggle",
                                name = L["Enable Bar"],
                                set = function (info, value)
                                        self.db.profile.bars.barst[icdIndex].enabled = not self.db.profile.bars.barst[icdIndex].enabled
                                        if (not self.icdBars[icdIndex]) then
                                            self:CreateICDBars()
                                        elseif (self.icdBars[icdIndex] and not self.icdBars[icdIndex]:IsShown()
                                        and self.db.profile.bars.barst[icdIndex].enabled and not self.db.profile.bars["lock"]) then
                                            self.icdBars[icdIndex]:Show()
                                        else
                                            self.icdBars[icdIndex]:Hide()
                                        end
                                        self:Updated(icdIndex,self.db.profile.bars.barst[icdIndex].enabled,false)
                                    end,
                                get = function (info) return self.db.profile.bars.barst[icdIndex].enabled end,
                                order = 4,
                            },
                            font = {
                                type = "select",
                                name = L["Font"],
                                set = function (info, value) varUpdate(icdIndex, "font", value) end,
                                get = function (info) return self.db.profile.bars.barst[icdIndex].font end,
                                values = function()
                                        self.fonts = self.fonts or {}
                                        wipe(self.fonts)
                                        for _, name in pairs(SM:List("font")) do 
                                            self.fonts[name] = name
                                        end
                                        return self.fonts
                                    end,
                                order = 5,
                            },
                            fontsize = {
                                type = "range",
                                name = L["Fontsize"],
                                set = function (info, value) varUpdate(icdIndex, "fontsize", value) end,
                                get = function (info) return self.db.profile.bars.barst[icdIndex].fontsize end,
                                min = 10,
                                max = 40,
                                step = 1,
                                order = 6,
                            },
                            iconSize = {
                                type = "range",
                                name = L["Icon size"],
                                desc = L["Set size of the icon"],
                                set = function (info, value) varUpdate(icdIndex, "iconsize", value) end,
                                get = function (info) return self.db.profile.bars.barst[icdIndex].iconsize end,
                                min = 15,
                                max = 50,
                                step = 1,
                                order = 7,
                            },
                            iconMarg = {
                                type = "range",
                                name = L["Icon margin size"],
                                desc = L["Set margin size of the icon"],
                                set = function (info, value) varUpdate(icdIndex, "iconmargin", value + 5) end,
                                get = function (info) return (self.db.profile.bars.barst[icdIndex].iconmargin - 5) end,
                                min = 0,
                                max = 20,
                                step = 1,
                                order = 8,
                            },
                            bonusToggle = {
                                type = "toggle",
                                name = L["Enable Bonus Icons"],
                                desc = L["Enable/Disable icon of Embroidiers or ashen band rings"],
                                get = function (info) return self.db.profile.bars.barst[icdIndex].bonuses end,
                                set = function (info, value) varUpdate(icdIndex, "bonuses", value) end,
                                order = 9,
                            },
                            ResetICD = {
                                type = "execute",
                                name = L["Reset ICD Bars"],
                                desc = L["This will reset all ICD Bars settings!"],
                                func = function() self:ResetDefines(9) end,
                                order = 10,
                            },
                        },
                    },
                },
            },
        },
    }
    
    self.options.args.profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
    LibStub("AceConfig-3.0"):RegisterOptionsTable("SimplyAlerts", self.options)
    LibStub("AceConfigDialog-3.0"):AddToBlizOptions("SimplyAlerts", "SimplyAlerts")
end

