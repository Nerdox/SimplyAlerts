SimplyAlerts = LibStub("AceAddon-3.0"):NewAddon("SimplyAlerts", "AceEvent-3.0", "AceConsole-3.0", "AceTimer-3.0")
local  L, SM = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true), LibStub("LibSharedMedia-3.0")

function SimplyAlerts:OnInitialize()
    self.total = { mark=0,afk=0,rmnd=0,barco={} }
    self.onetime = { petalive = true, reminders={}, counters={["enemy"]={}, ["me"]={}}, lastmind=1, barac={["player"]={}}, items={} }
    self.playerName = GetUnitName("player", true)

    self:RefreshEvents()
    self:GetDefines()
    self.db = LibStub("AceDB-3.0"):New("SimplyAlertsDB", self.defaults, true)
    self:GetOptions()
    -- self.db.profile.bars.barst.player.cache[self.playerName] = {}

    self.alert = alert or self:CreateFrame()
    self:UpdateFrame()

    ChatFrame1:AddMessage("Simply Alerts by Nerdox. Type /simplya for options.",0,1,1)
end

local function OnUpdate(self, elapsed)
    -- SimplyAlerts:AntiAFK(elapsed) -- Disabled due to restriction of WoW Lua
    SimplyAlerts:PetMarker(elapsed)
    SimplyAlerts:Reminder(elapsed)
    SimplyAlerts:BarTextUp(elapsed)
end

function SimplyAlerts:RefreshEvents()
    self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    self:RegisterEvent("CHAT_MSG_BG_SYSTEM_NEUTRAL")
    self:RegisterEvent("CHAT_MSG_RAID_BOSS_EMOTE")
    self:RegisterEvent("UPDATE_BATTLEFIELD_STATUS")
    self:RegisterEvent("LOADING_SCREEN_DISABLED")
    self:RegisterEvent("PLAYER_EQUIPMENT_CHANGED")
    self:RegisterEvent("UNIT_INVENTORY_CHANGED")
    self:RegisterEvent("PARTY_MEMBERS_CHANGED")
    self:RegisterEvent("ZONE_CHANGED_NEW_AREA")
    self:RegisterEvent("PLAYER_TARGET_CHANGED")
    self:RegisterEvent("PLAYER_FOCUS_CHANGED")
    self:RegisterEvent("PLAYER_LOGIN")
    --self:RegisterEvent("FRIENDLIST_UPDATE") -- Disabled due to restriction of WoW Lua

    hooksecurefunc("UnitFrameHealthBar_Update", function(statusbar, unit)
        return self:UpdateClassFrame(statusbar)
    end)

    hooksecurefunc("HealthBar_OnValueChanged", function(statusbar, value)
        return self:UpdateClassFrame(statusbar)
    end)

    hooksecurefunc("TextStatusBar_UpdateTextString", function(statusbar)
        return self:UpdateStatusBar(statusbar)
    end)
end

function SimplyAlerts:CHAT_MSG_BG_SYSTEM_NEUTRAL(event)
    SimplyAlerts:AutoMarker(arg1)
    SimplyAlerts:SpellReport(arg1)
    SimplyAlerts:ArenaBars(arg1)
end

function SimplyAlerts:CHAT_MSG_RAID_BOSS_EMOTE(event)
    SimplyAlerts:ArenaBars(arg1)
end

function SimplyAlerts:UPDATE_BATTLEFIELD_STATUS(event)
    -- SimplyAlerts:EnterBattleground(event) -- Disabled due to restriction of WoW Lua
    -- SimplyAlerts:InitDodger() -- Disabled due to restriction of WoW Lua
end

function SimplyAlerts:LOADING_SCREEN_DISABLED(event)
    -- SimplyAlerts:JoinBattleground(event) -- Disabled due to restriction of WoW Lua
end

function SimplyAlerts:PARTY_MEMBERS_CHANGED(event)
    SimplyAlerts:PartyBars()
end

function SimplyAlerts:PLAYER_EQUIPMENT_CHANGED(arg1, arg2)
    SimplyAlerts:UpdatePlayerTrinkets(arg1, arg2)
    SimplyAlerts:UpdateFriendlyBonusIcons("player")
end

function SimplyAlerts:UNIT_INVENTORY_CHANGED(time, arg1)
    SimplyAlerts:UpdatePartyTrinkets(time, arg1)
    SimplyAlerts:UpdateFriendlyBonusIcons(arg1)
end

function SimplyAlerts:ZONE_CHANGED_NEW_AREA()
    SimplyAlerts:StopAllTimers()
    SimplyAlerts:ArenaBars("Team wins!")
    SimplyAlerts:RefreshEvents()

    if (not self.onetime.petalive) then
        self.onetime.petalive = true
    end
end

function SimplyAlerts:PLAYER_TARGET_CHANGED()
    SimplyAlerts:UpdateClassFrame(TargetFrame)
end

function SimplyAlerts:PLAYER_FOCUS_CHANGED()
    SimplyAlerts:UpdateClassFrame(FocusFrame)
end

function SimplyAlerts:FRIENDLIST_UPDATE()
end

function SimplyAlerts:PLAYER_LOGIN()
    self.icdBars = {}
    self:CreateICDBars()
end

--function SimplyAlerts:UNIT_TARGET(event)
--    --if arg1 == "focus" then
--    --    self:UpdateClassFrame(FocusFrameToT)
--    --elseif arg1 == "target" then
--    --    self:UpdateClassFrame(TargetFrameToT)
--    --end
--end

-----------------------------------------------------
--                     SPECIAL                     --
-----------------------------------------------------

function SimplyAlerts:IsArena() 
    local isArena, isRegistered = IsActiveBattlefieldArena()

    return isArena or isRegistered
end

function SimplyAlerts:AddMessage(module, message)
    DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts" .. module .. ":|r " .. message)
end

function SimplyAlerts:Updated(settings, setting, novalue)
    local string
    if (novalue) then
        string = setting
    else
        string = tostring(setting)
    end

    string = string.upper(string)
    ChatFrame1:AddMessage("Option " .. string.upper(settings) .. " changed to: " .. string,0,1,1)
end

function SimplyAlerts:UnitCanHavePet(unit)
    return UnitClass(unit) == "Warlock" or UnitClass(unit) == "Death Knight" or UnitClass(unit) == "Hunter";
end

function SimplyAlerts:IsPartyMember(player)
    for unitIndex=1, GetNumPartyMembers() do
        if (player == UnitName("party" .. unitIndex)) then
            return true
        end
    end

    return false
end

function SimplyAlerts:SpellName(spellId)
    local spellName = GetSpellInfo(spellId)

    if spellName then
        return spellName
    end

    return false
end

function SimplyAlerts:SpellID(spell)
    local itemLink = GetSpellLink(spell)

    if itemLink then
        return tonumber(select(3, strfind(itemLink, "spell:(%d+)|")))
    end

    return false
end

function SimplyAlerts:ItemIcon(itemId)
    if (itemId == 25561) then 
        return "Interface\\Icons\\Inv_misc_questionmark"
    end

    if (itemId) then
        local iconPath = GetItemIcon(itemId) 

        if iconPath then
            return iconPath
        end

        return self:SpellIcon(itemId)
    end

    return "Interface\\Icons\\Inv_misc_questionmark"
end

function SimplyAlerts:SpellIcon(spellId)
    if (spellId) then
        local icon = select(3, GetSpellInfo(spellId))
        
        if icon then
            return icon
        end
    end

    return "Interface\\Icons\\Inv_misc_questionmark"
end

function SimplyAlerts:ItemID(link)
    if link then
        return tonumber(select(3, strfind(link, "item:(%d+):")))
    end

    return false
end

function SimplyAlerts:GetPowerName(id)
    local powers = {"Mana", "Rage", "Focus (hunter's pet)", "Energy", "Combo Points", "Runes", "Runic Power"}
    return powers[id+1]
end

function SimplyAlerts:LastValue(tbl)
    local count = 0
    for _ in pairs(tbl) do count = count + 1 end

    return count
end

function SimplyAlerts:Contains(tbl, value)
    for k,v in ipairs(tbl) do
        if (v == value) then
            return true
        end
    end

    return false
end

function SimplyAlerts:ConvertSettingsToProfile()
    local hasAnyUncovertedSettings = false
    local targetProfileName = "Automatic copy"

    for key, _ in pairs(self.defaults.profile) do
        if SimplyAlertsDB[key] then
            hasAnyUncovertedSettings = true
            break
        end
    end

    if not hasAnyUncovertedSettings then
        return DEFAULT_CHAT_FRAME:AddMessage("SimplyAlerts: There are no data to convert")
    end

    self.db:SetProfile(targetProfileName)

    local function fillMissingData(dest, src)
        for key, value in pairs(src) do
            if type(value) == "table" then
                -- If the key exists in dest and is a table, recurse
                if type(dest[key]) == "table" then
                    fillMissingData(dest[key], value)
                else
                    -- Otherwise, directly copy the table from src
                    dest[key] = value
                end
            else
                -- If the key doesn't exist in dest or its value is nil, copy it from src
                if dest[key] == nil then
                    dest[key] = value
                end
            end
        end
    end

    fillMissingData(self.db.profile, SimplyAlertsDB)
    fillMissingData(self.db.profile, self.defaults.profile)
    self.db.profile.profiles = nil
    self.db.profile.profileKeys = nil

    for key, _ in pairs(self.defaults.profile) do
        SimplyAlertsDB[key] = nil
    end

    DEFAULT_CHAT_FRAME:AddMessage("SimplyAlerts: Profile converted to new profile with name " .. targetProfileName)
    DEFAULT_CHAT_FRAME:AddMessage("To load settings please use /reload")
end

-----------------------------------------------------
--                 COMBATLOG ALERTS                --
-----------------------------------------------------

function SimplyAlerts:COMBAT_LOG_EVENT_UNFILTERED(event, ...)
    if (arg2 == "SPELL_MISSED") then
        self:GetExtraMsgs(arg9, arg12, arg7, arg4)

        if (arg7 == self.playerName and UnitClass("player") == "Warrior" and (arg12 == "DODGE" or arg12 == "PARRY" or arg12 == "BLOCK")) then
            self:RemindMe(57823)
        end

        if (self.playerName == arg4 or self.playerName == arg7) then
            self:CountSpell(arg12, arg9, arg4, arg7)
        end
    end

    if ((arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH" or (arg2 == "SPELL_CAST_SUCCESS" and arg9 ~= 42292)) and self.ItemProcs[arg9]) then
        if (arg4 == self.playerName and self.db.profile.bars.barst.player.enabled) then
            --DEFAULT_CHAT_FRAME:AddMessage("debug 1")
            local item = self:GetProcItemID(arg9, arg4)
            local slot = self:GetItemSlot(item)
            --DEFAULT_CHAT_FRAME:AddMessage("debug 2")
            if (self.ItemProcs[arg9]["spec"] > 0) then
                slot = self.ItemProcs[arg9]["spec"]
            end

            self:StartTimer(self.icdBars.player[slot], "player", item, arg9)
            --DEFAULT_CHAT_FRAME:AddMessage("debug 3")
        else
            if (self:IsArena()) then
                --DEFAULT_CHAT_FRAME:AddMessage("debug 4")
                local unit = self:PlayerToUnit(arg4)

                if (unit ~= false and self.db.profile.bars.barst[unit].enabled and self.onetime.items[unit]) then
                    --DEFAULT_CHAT_FRAME:AddMessage("debug 5")
                    local item = self:GetProcItemID(arg9, arg4) 

                    if (self.ItemProcs[arg9]["spec"] > 0) then
                        --DEFAULT_CHAT_FRAME:AddMessage("debug 6")
                        if (not self.db.profile.bars.barst[unit].bonuses) then
                            return
                        end

                        self:HandleItemStart(unit, self.ItemProcs[arg9]["spec"], item, arg9, self.ItemProcs[arg9]["spec"], false, true)
                    else
                        --DEFAULT_CHAT_FRAME:AddMessage("debug 7")
                        for slot=13,14 do
                            self:HandleItemStart(unit, slot, item, arg9, slot, false, false)
                        end
                    end
                end
            end
        end
    end

    if (arg7 == self.playerName) then
        self:GetInfoMsg(arg9, arg2, arg12, arg7, arg4)

        if ((arg2 == "SPELL_DISPEL" or arg2 == "SPELL_STOLEN" or arg2 == "SPELL_AURA_REMOVED") and arg4 == self.playerName) then
            if (self:Contains(self.db.profile.reminder.spells, arg9) and not self:Contains(self.onetime.reminders, arg9)) then
                self:SetRemind(arg9)
            end
        end

        if (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH") then
            if (arg4 ~= self.playerName) then
                self:GetControlMsg(arg9)
            end

            if (self:Contains(self.onetime.reminders, arg9)) then
                self:UnRemind(arg9)
            end
        end

        if (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_APPLIED_DOSE") and arg9 == 71905 then
            self:CancelSoulFragments(arg10)
        end
    end

    if (arg2 == "SPELL_CAST_SUCCESS" and arg9 == 42292) then -- pvp trinket
        local unit = self:PlayerToUnit(arg4)

        if (unit) then
            if (self.icdBars[unit]) then
                local itemId, slot = UnitFactionGroup(unit) == "Horde" and 51378 or 51377, 14

                if (unit == "player") then
                    itemId = self:GetProcItemID(88888, self.playerName)
                    slot = self:GetItemSlot(itemId)
                elseif (self.onetime.items[unit][13] == itemId) then
                    slot = 13
                end

                if (self.onetime.items[unit][slot] ~= 51378 and self.onetime.items[unit][slot] ~= 51377 and unit ~= "player") then
                    self:UpdateIcon(unit, slot, itemId)
                end
                
                self:StartTimer(self.icdBars[unit][slot], unit, self.onetime.items[unit][slot], arg9)
            end
        end
    end
end

-----------------------------------------------------
--                  Test Function                  --
-----------------------------------------------------

function SimplyAlerts:Test()
    SimplyAlerts:GetInfoMsg(29166, "SPELL_DISPEL", 29166, self.playerName, self.playerName)
    SimplyAlerts:GetExtraMsgs(29166, "REFLECT", self.playerName, self.playerName)
    SimplyAlerts:GetInfoMsg(29166, "SPELL_AURA_REMOVED", 29166, self.playerName, self.playerName)
    SimplyAlerts:GetExtraMsgs(29166, "DODGE", self.playerName, self.playerName)
    SimplyAlerts:GetInfoMsg(29166, "SPELL_AURA_APPLIED", 29166, self.playerName, self.playerName)
    SimplyAlerts:GetControlMsg(61780)
end

function SimplyAlerts:DebugArch(archive)
    if (not self.onetime.debugarch) then
        self.onetime.debugarch = {}
    end

    local str = "|cFFFF5A5A" .. date("%H:%M:%S") .. " ->|r"
    for i,v in pairs(archive) do
        str = str .. " |cff33ff99" .. string.sub(i,2) .. ":|r " .. v
    end
    table.insert(self.onetime.debugarch, str)
end

function SimplyAlerts:DebugRep()
    print("|cff33ff99-\\/ SimplyAlerts Debug \\/-|r")
    for _,v in ipairs(self.onetime.debugarch) do
        print(v)
    end
    self.onetime.debugarch = {}
end

-----------------------------------------------------
--                   CMDS, etc..                   --
-----------------------------------------------------

SLASH_SIMPLYALERTS1 = "/simplya"
SLASH_SIMPLYALERTS2 = "/simplyalerts"
SlashCmdList["SIMPLYALERTS"] = function(msg)
    msg = msg or ""
    
    local cmd, arg = string.split(" ", msg, 2)
    cmd = string.lower(cmd or "")
    arg = string.lower(arg or "")
    
    local self = SimplyAlerts
    
    -- Disabled due to restriction of WoW Lua
    -- if (cmd == "autorbg" and arg == "") then
    --     self.db.profile.autorbg = not self.db.profile.autorbg
    --     self:Updated("autorbg",self.db.profile.autorbg,false)
    --     self:JoinBattleground()
    -- else
    
    if (cmd == "test") then
        self:Test()
    elseif (cmd == "dbg" or cmd == "debug") then
        self:DebugRep()
    elseif (cmd == "ui") then
        InterfaceOptionsFrame_OpenToCategory("SimplyAlerts")
    elseif (cmd == "convert") then
        self:ConvertSettingsToProfile()
    else
        DEFAULT_CHAT_FRAME:AddMessage("|cff33ff99SimplyAlerts commands are|r:")
        DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33-> /simplya|r ui")
        DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33-> /simplya|r test")
        DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33-> /simplya|r convert -- Convert old settings to new profile system")

        -- Disabled due to restriction of WoW Lua
        -- DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33-> /simplya|r autorbg |cFFFF5A5Arequires LuaUnlocker|r")
    end
end

local frame = CreateFrame("Frame")
frame:RegisterEvent("ADDON_LOADED")
frame:SetScript("OnEvent", function(self, event, addon)
    if(addon == "SimplyAlerts") then
        self:UnregisterEvent("ADDON_LOADED")
    end
end)
frame:SetScript("OnUpdate", OnUpdate)