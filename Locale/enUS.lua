﻿local L = LibStub("AceLocale-3.0"):NewLocale("SimplyAlerts", "enUS", true)
if not L then return end
    
    L["Informations"] = true
    --
	L["Lang_Info"]="Version 1.3\n\nMade by Nerdox, 2017.\nhttps://gitlab.com/Nerdox/SimplyAlerts"
    L["Credits, author, reset.."] = true

  L["RELOAD_UI"] = " (reload UI may be needed)"

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Extra"] = true
    L["Special functions"] = true
      L["Cancel Soul Fragments"] = true
      L["Automatically cancel soul fragments at 9 stacks"] = true
    --
    L["Raid Markers"] = true
      L["Auto raid target icons"] = true
      L["Automatic raid marks before arena starts"] = true
      --
      L["Auto pet remarker"] = true
      L["Automatically remark pets after they died and were revived"] = true
      --
      L["Select unit to set party mark"] = true
      --
      L["Select mark for selected unit"] = true
      --
      L["Reset Markers"] = true
      L["This will reset all Markers settings!"] = true
    --
    L["Auto random battlegrounds (requires LuaUnlocker!)"] = true
    L["Automatic random battlegrounds"] = true
    --
    L["Class color frames"] = true
    L["Colored health bars and name background with class color of unit"] = true
      L["Enable coloring"] = true
      --
      L["Coloring mode"] = true
      L["Select a mode in which the frames will be colored"] = true
      --
      L["Modify a field"] = true
      L["Select a field to modify its behaviour"] = true
      --
      L["Disable Coloring"] = true
      L["Disable coloring of currently selected field in frames"] = true
      --
      L["Translucency PLAYER"] = true
      L["Set translucency for ONLY PLAYER frames"] = true
      --
      L["Translucency OTHER"] = true
      L["Set translucency for all OTHER frames except PLAYER FRAMES"] = true
      --
      L["Reset Colored Frames"] = true
      L["This will reset all settings for Class Colored Frames!"] = true
    --
    L["Health and Power bars"] = true
    L["Adds percentages to the left side of haelth and power bars"] = true
      L["Enable percentages"] = true
      --
      L["Specific frame"] = true
      L["Update values for specific frames"] = true
      --
      L["Enable for specific frame"] = true
      L["Enable percentages for specific frame"] = true
      --
      L["Enable power type"] = true
      --
      L["Enable only for specific power types"] = true
      L["Select option for which power type percentages will be shown"] = true
      --
      L["Percentages offset"] = true
      L["Set offset for percentage value"] = true
      --
      L["Amount offset"] = true
      L["Set offset for amount of current power (mana, rage)"] = true
      --
      L["Replace max value"] = true
      L["Replace max value with only current value for disabled power types (e.g: 20 / 100 -> 20)"] = true
      --
      L["Reset Settings"] = true
      L["This will reset all \"Health and Power bars\" settings"] = true
    --
    L["Cancel Soul Fragments"] = true
    L["Automatically cancel Soul Fragments at 9 stacks"] = true
      L["Notify me"] = true
      L["Send me local message when Soul Fragments are canceled"] = true
      --
      L["Reset Cancel Soul Fragments"] = true
      L["This will reset all Cancel Soul Fragments settings!"] = true
    --
    L["Arena Dodger"] = true
    L["Do you want to not lose rating? Dodge them"] = true
      L["Enable automatic queue leave when selected friends are out of the arena"] = true
      --
      L["Friendlist Note"] = true
      L["Enable dodging only by specific note"] = true
      --
      L["Note String"] = true
      L["Insert specific friendlist note string to be dodged"] = true
      --
      L["Ignore AFK"] = true
      L["Ignore players with /away status"] = true
      --
      L["Reset Arena Dodger"] = true
      L["This will reset all Arena Dodger settings!"] = true


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Spell Notifications"] = true
    L["Configure notifications"] = true
    --
    L["Toggle CC Notifications"] = true
    L["Messages about CC (like 'SEDUCED') to group members"] = true
    --
    L["Message type"] = true
    L["Where to display ALL (group) announcement messages"] = true
    --
    L["Defaults"] = true


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Aura Notifications (Local)"] = true
    L["Aura messages (like spell dispells) to player (you)"] = true
    --
    L["Alert Settings"] = true
      L["Font"] = true
      L["Fontsize"] = true
      L["Hold Time"] = true
      L["Lock"] = true
      L["Reset Alert Settings"] = true
      L["This will reset all Alert settings including position!"] = true
    --
    L["Toggle Aura Notifications"] = true
    --
    L["Toggle Spell Icon"] = true
    L["Spell icon in aura spell notification"] = true
    --
    L["Spell icon size"] = true
    L["Set size of spell icon"] = true
    --
    L["Select Uppercase"] = true
    L["Select where you want to use uppercases for spell name"] = true
      L["Both Disabled"] = true
      L["Both Enabled"] = true
      L["Only 'Dispells'"] = true
      L["Only 'Down'"] = true
    --
    L["Set 'Dispell' Message"] = true
    L["Set 'dispell' message, use '%s' for spell name"] = true
    --
    L["Set 'Down' Message"] = true
    L["Set 'down' message, use '%s' for spell name"] = true
    --
    L["Arena Only"] = true
    L["Enable Local Notifications only in arena"] = true
    --
    L["Reset Local Notifications"] = true
    L["This will reset all Local Notifications settings!"] = true


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Aura Notifications (Group)"] = true
    L["Aura messages (like spell dispells) to group members"] = true
    --
    L["Set 'Up' Message"] = true
    L["Set 'up' message, use '%s' for spell name"] = true
      L["Only 'Up'"] = true
    --
    L["Delete announced spells"] = true
    L["Select spell which will be deleted from announced spells list"] = true
    --
    L["Add spells for announcements"] = true
    L["Insert SpellID to add spell to announcement spell list"] = true
    --
    L["Enable Chat Notifications only in arena"] = true
    --
    L["Reset Group Notifications"] = true
    L["This will reset all Group Notifications settings!"] = true


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Spell Status (Group)"] = true
    L["Spell messages about misses/dodges, etc.."] = true
    --
    L["Spell Status"] = true
    --
    L["Filtered Groups"] = true
    L["Select which group of players will be checked"] = true
      L["Only Me"] = true
      L["Me + Party members"] = true
      L["Only Party members"] = true
      L["All"] = true
    --
    L["Enable Spell Status alerts only in arena"] = true
    --
    L["Enable/Disable spell miss type"] = true
    L["Select miss type which will have enabled/disabled announce"] = true
    --
    L["Reset Spell Status"] = true
    L["This will reset all Spell Status settings!"] = true
    --
    L["Spell Alerter"] = true
    L["Messages about how much misses/dodges.. you did (Arena Only)"] = true
    --
    L["Toggle Spell Alerter"] = true
    --
    L["Types"] = true
    L["Select which miss types will be counted"] = true
    --
    L["Chat Type"] = true
    L["Select where report should be reported"] = true
    --
    L["Reset Spell Alerter"] = true
    L["This will reset all Spell Alerter settings!"] = true


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["Spell Reminder (Local)"] = true
    L["Remind buffs that faded / were dispelled.."] = true
    --
    L["Spell Reminder"] = true
    --
    L["Interval"] = true
    L["Interval between alerts"] = true
    --
    L["Delete reminder spells"] = true
    L["Select spell which will be deleted from reminder spells list"] = true
    --
    L["Add spells to reminder"] = true
    L["Insert SpellID to add spell to reminder spell list"] = true
    --
    L["Enable Spell Reminder alerts only in arena"] = true
    --
    L["Reset Spell Reminder"] = true
    L["This will reset all Spell Reminder settings!"] = true

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
    L["ICD Bar (Local)"] = true
    L["Internal Cooldowns Bar"] = true
    --
    L["Global Settings"] = true
    L["Every single change will be set to every bar types (except toggling the bars & cd text)"] = true
    --
    L["Lock Bars"] = true
    L["Lock activated bars"] = true
    --
    L["Select bar type to edit"] = true
    --
    L["Enable Bar"] = true
    --
    L["Enable/Disable bar type"] = true
    --
    L["Icon size"] = true
    L["Set size of the icon"] = true
    --
    L["Icon margin size"] = true
    L["Set margin size of the icon"] = true
    --
    L["Enable Bonus Icons"] = true
    L["Enable/Disable icon of Embroidiers or ashen band rings"] = true
    --
    L["Enable CD Text"] = true
    L["Enable/Disable text of cooldowns"] = true
    --
    L["Reset ICD Bars"] = true
    L["This will reset all ICD Bars settings!"] = true

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
