-----------------------------------------------------
--                    AUTO RBGS                    --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:JoinBattleground(event)
    if (UnitInBattleground("player") == (false or nil)) then
        if (self.db.profile.autorbg) then
            DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33Auto queing RBG|r")
            JoinBattlefield(0)
        end
    end
end

function SimplyAlerts:EnterBattleground(event)
    if (self.db.profile.autorbg) then
        for queueIndex=1, MAX_BATTLEFIELD_QUEUES do
            status, mapName, instanceID = GetBattlefieldStatus(queueIndex)
            if (status == "confirm") then
                if (AcceptBattlefieldPort(queueIndex, 1)) then
                    DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33Auto entering RBG|r")
                else
                    DEFAULT_CHAT_FRAME:AddMessage("|cffff4747No LuaUnlocker detected!|r")
                    
                    self.db.profile.autorbg = false
                    self:Updated("autorbg",self.db.profile.autorbg,false)
                end
            end
        end
    end
end

function SimplyAlerts:AntiAFK(elapsed)
    if (self.db.profile.autorbg and UnitInBattleground("player")) then
        self.total.afk = self.total.afk + elapsed
        if (self.total.afk >= 30) then
            if (IsMounted()) then
                Dismount()
            end
            if (UnitIsDead("player")) then
                MoveForwardStop(GetTime()*1000 + 2000)
                RepopMe()
            end
            if (UnitAura("player", "Idle")) then
                MoveForwardStart(GetTime()*1000 + 1000)
                DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33..idle detected|r moving forward..")
            end

            DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33Auto anti-afked|r")
            JumpOrAscendStart()
            self.total.afk = 0
        end
    end
end