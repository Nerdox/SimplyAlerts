-----------------------------------------------------
--                     SPECIAL                     --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:AutoMarker(arg1)
    if (self:IsArena() and self.db.profile.markers["marker"]) then
        if (string.find(arg1, "Fifteen seconds until the Arena battle begins!")) then
            local markers = self.db.profile.markers.units

            SetRaidTarget("player", markers["player"])
            if UnitExists("player-pet") then
                SetRaidTarget("player-pet", markers["player-pet"])
            end

            for unitIndex=1, GetNumPartyMembers() do
                SetRaidTarget("party" .. unitIndex, markers["party" .. unitIndex])
                if UnitExists("party" .. unitIndex .. "-pet") then
                    SetRaidTarget("party" .. unitIndex .. "-pet", markers["party" .. unitIndex .. "-pet"])
                end
            end
        end
    end
end

function SimplyAlerts:PetMarker(elapsed)
    if (self:IsArena() and self.db.profile.markers["petmark"]) then
        self.total.mark = self.total.mark + elapsed

        if (self.total.mark > 15) then
            for unitIndex=1, GetNumPartyMembers() do
                if self.onetime.petalive then
                    if (self:UnitCanHavePet("player") and not UnitExists("player-pet")
                    or self:UnitCanHavePet("party" .. unitIndex) and not UnitExists("party" .. unitIndex .. "-pet")) then
                        self.onetime.petalive = nil
                    else    
                        self.onetime.petalive = true
                    end
                end

                if not self.onetime.petalive then
                    if ((self:UnitCanHavePet("player") and UnitExists("player-pet") and self:UnitCanHavePet("player"))
                    or (self:UnitCanHavePet("party" .. unitIndex) and UnitExists("party" .. unitIndex .. "-pet") and self:UnitCanHavePet("party" .. unitIndex))) then
                        SetRaidTarget("party" .. unitIndex .. "-pet", self.db.profile.markers.units["party" .. unitIndex .. "-pet"])
                        self.onetime.petalive = true
                    end
                end
            end

            self.total.mark = 0
        end
    end
end

function SimplyAlerts:UpdateClassFrame(frame)
    if not UnitIsPlayer(frame.unit) then
        return
    end

    local settings = self.db.profile.classframes

    if settings.enabled then
        local _, class = UnitClass(frame.unit)

        if class then
            local c = RAID_CLASS_COLORS[class]
            local colorIndex = "other"

            if frame.unit == "player" then
                colorIndex = "player"

                if not PlayerFrame.nameBackground then
                    PlayerFrame.nameBackground = PlayerFrame:CreateTexture()
                    PlayerFrame.nameBackground:SetPoint("TOPLEFT", PlayerFrameBackground)
                    PlayerFrame.nameBackground:SetPoint("BOTTOMRIGHT", PlayerFrameBackground, 0, 22)
                    PlayerFrame.nameBackground:SetTexture(TargetFrameNameBackground:GetTexture())
                    frame = PlayerFrame
                end
            end

            if (settings.mode == "all" or settings.mode == "name") and frame.nameBackground then
                if not settings.translucency.name[colorIndex].disabled then
                    frame.nameBackground:SetVertexColor(c.r,c.g,c.b, settings.translucency.name[colorIndex].val)
                end
            end

            if (settings.mode == "all" or settings.mode == "health") and not settings.translucency.health[colorIndex].disabled then
                if frame.healthbar then
                    frame.healthbar:SetStatusBarColor(c.r,c.g,c.b, settings.translucency.health[colorIndex].val)
                else 
                    frame:SetStatusBarColor(c.r,c.g,c.b, settings.translucency.health[colorIndex].val)
                end
            end
        end
    end


    if (not settings.enabled or (settings.mode ~= "all" and settings.mode ~= "name") or settings.translucency.name.player.disabled) 
        and PlayerFrame.nameBackground then
        PlayerFrame.nameBackground:SetVertexColor(1,1,1, 0)
    end

    if not settings.enabled or (settings.mode ~= "all" and settings.mode ~= "health")
        or settings.translucency.health.player.disabled then
        local r,_,b,_ = PlayerFrame.healthbar:GetStatusBarColor()

        if r > 0 or b > 1 then 
            PlayerFrame.healthbar:SetStatusBarColor(0,1,0, 1)
        end
    end
end

function SimplyAlerts:CancelSoulFragments(soulFragmentName)
    if (not self.db.profile.autoCancelSoulFragment.enabled) then
        return
    end

    local _, _, _, count = UnitBuff("player", soulFragmentName)

    if count == 9 then
        CancelUnitBuff("player", soulFragmentName)

        if self.db.profile.autoCancelSoulFragment.notify then
            DEFAULT_CHAT_FRAME:AddMessage("|cff33ff33SimplyAlerts:|r Canceled Soul Fragments" )
        end
    end
end