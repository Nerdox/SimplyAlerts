-----------------------------------------------------
--            INTERNAL COOLDOWN TRACKER            --
-----------------------------------------------------

local L, SM = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true), LibStub("LibSharedMedia-3.0")
local self = SimplyAlerts

function SimplyAlerts:haha() -- test function
    self:StartTimer(self.icdBars.player[13], "player", 50343, 71541)
end


function SimplyAlerts:CreateICDBars()
    for k,_ in pairs(self.db.profile.bars.barst) do
        if (self.db.profile.bars.barst[k].enabled and not self.icdBars[k]) then
            self.icdBars[k] = CreateFrame("Frame", nil, UIParent)
            self.icdBars[k]:SetWidth(120)
            self.icdBars[k]:SetHeight(30)
            self.icdBars[k]:SetClampedToScreen(true)
            self.icdBars[k]:Show()

            local text = self.icdBars[k]:CreateFontString(nil,"ARTWORK")
            text:SetFont(STANDARD_TEXT_FONT,18,"OUTLINE")
            text:SetTextColor(1,1,0,1)
            text:SetPoint("RIGHT",self.icdBars[k],"CENTER",0,self.db.profile.bars.barst[k].iconsize-10)
            text:SetText(k)
            self.icdBars[k].txt = text

            self:CheckTrinkets(k, true)

            local x = -45
            for slot,item in pairs(self.onetime.items[k]) do
                self:AddIcons(item, self:GetItemCD(item), x, k, slot)
                x = x + 30
            end

            --if (k == "player") then self:BonusIcons(x) end

            self.onetime.barac[k] = {}
            self:UpdateICDBar(k)
        end
    end
end

function SimplyAlerts:AddIcons(item, time, x, k, slot)
    local btn = CreateFrame("Frame",nil,self.icdBars[k])
    btn:SetWidth(30)
    btn:SetHeight(30)
    btn:SetPoint("CENTER",self.icdBars[k],"CENTER",x,0)
    btn:SetFrameStrata("LOW")

    local border = CreateFrame("Frame",nil,btn)
    border:SetFrameLevel(2)
    border:SetPoint("TOPLEFT",btn,"TOPLEFT",-4,4)
    border:SetPoint("BOTTOMRIGHT",btn,"BOTTOMRIGHT",4,-4)
    border:SetBackdrop({ 
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16, 
        edgeSize = 16, 
        insets = {
            left = 4,
            right = 4,
            top = 4,
            bottom = 4,
        },
    })
    border:SetBackdropColor(1, 1, 1, 0)
    border:SetBackdropBorderColor(0, 1, 0, 1)
    
    local cd = CreateFrame("Cooldown",nil,btn)
    cd:SetAllPoints(true)
    cd:SetFrameStrata("MEDIUM")

    local texture = btn:CreateTexture(nil,"BACKGROUND")
    texture:SetAllPoints(true)
    texture:SetTexture(self:ItemIcon(item))
    texture:SetTexCoord(0.07,0.9,0.07,0.90)

    local text = cd:CreateFontString(nil,"ARTWORK")
    text:SetFont(SM:Fetch("font"),18,"THICKOUTLINE")
    text:SetTextColor(1,1,0,1)
    text:SetPoint("CENTER",btn,0)

    btn.texture = texture
    btn.border = border
    btn.text = text
    btn.coold = time
    btn.cd = cd
    btn.pos = x
    btn.id = item
    btn.duration = time

    self.icdBars[k][slot] = btn
end

function SimplyAlerts:BonusIcons(unit)
    local bonusSlots = {15, 12, 11}

    for _, slot in ipairs(bonusSlots) do
        local itemLink = GetInventoryItemLink(unit, slot)
        if slot == 11 then slot = 12 end

        if (itemLink) then
            if (not self.icdBars[unit][slot]) then
                local itemId, enchantId = itemLink:match("item:(%d+):(%d+):")
                local enchants = self:GetEnchantments()
                self.onetime.items[unit][slot] = 25561

                if (enchants[enchantId]) then
                    --DEFAULT_CHAT_FRAME:AddMessage("enchant2: " .. enchants[enchantId])
                    self.onetime.items[unit][slot] = enchants[enchantId]
                elseif (self:IsItemAshenBand(itemId)) then
                    self.onetime.items[unit][12] = tonumber(itemId)
                end
            end
        else
            self.onetime.items[unit][slot] = 25561
        end
    end
end

function SimplyAlerts:UpdateIcon(unit, slot, item, fhide, fshow)
    self.icdBars[unit][slot].texture:SetTexture(self:ItemIcon(item))
    self.icdBars[unit][slot].coold = self:GetItemCD(item)
    self.onetime.items[unit][slot] = item

    if fhide == true then
        self.icdBars[unit][slot]:Hide()
    end

    if fshow == true then
        self.icdBars[unit][slot]:Show()
    end
end

function SimplyAlerts:UpdateICDBar(unit)
    if (self.db.profile.bars.barst[unit].pos) then
        self.icdBars[unit]:SetPoint(unpack(self.db.profile.bars.barst[unit].pos))
    else
        self.icdBars[unit]:SetPoint("CENTER", UIParent, "CENTER")
    end
    self.icdBars[unit].txt:SetPoint("RIGHT",self.icdBars[unit],"CENTER",0,self.db.profile.bars.barst[unit].iconsize-10)

    local x = -45
    for slot=12,15 do
        if (self.icdBars[unit][slot]) then
            self.icdBars[unit][slot].text:SetFont(SM:Fetch("font",self.db.profile.bars.barst[unit].font),self.db.profile.bars.barst[unit].fontsize,"THICKOUTLINE")
            self.icdBars[unit][slot]:SetWidth(self.db.profile.bars.barst[unit].iconsize)
            self.icdBars[unit][slot]:SetHeight(self.db.profile.bars.barst[unit].iconsize)
            self.icdBars[unit][slot]:SetPoint("CENTER",self.icdBars[unit],"CENTER",x,0)
            x = x+self.db.profile.bars.barst[unit].iconsize+self.db.profile.bars.barst[unit].iconmargin

            if (not self.db.profile.bars.barst[unit].showtext) then
                self.icdBars[unit][slot].text:SetText("  ")
            end

            if (slot == 12 or slot == 15) then
                if (self.db.profile.bars.barst[unit].bonuses and (not self.db.profile.bars.lock or self.onetime.items[unit][slot] ~= 25561)) then
                    self.icdBars[unit][slot]:Show()
                else
                    self.icdBars[unit][slot]:Hide()
                end
            end
        end
    end

    if (self.db.profile.bars.lock) then
        self.icdBars[unit]:EnableMouse(false)
        self.icdBars[unit]:SetMovable(false)
        self.icdBars[unit].txt:Hide()

        if (not UnitExists(unit) and unit~="player") then
            self.icdBars[unit]:Hide()
        end
    else
        self.icdBars[unit]:EnableMouse(true)
        self.icdBars[unit]:SetMovable(true)
        self.icdBars[unit]:SetScript("OnMouseDown",function(self,button) if button == "LeftButton" then self:StartMoving() end end)
        self.icdBars[unit]:SetScript("OnMouseUp",function(self,button) if button == "LeftButton" then self:StopMovingOrSizing() SimplyAlerts:SaveBarPos(unit) end end)
        self.icdBars[unit].txt:Show()
        self.icdBars[unit]:Show()
    end
end

function SimplyAlerts:SaveBarPos(unit)
    if (self.db.profile.bars.barst[unit].enabled) then
        self.db.profile.bars.barst[unit].pos = {self.icdBars[unit]:GetPoint()}
    end
end

function SimplyAlerts:StartTimer(ref, unit, item, spell)
    if (self.onetime.barac[unit][item] == nil) then
        --DEFAULT_CHAT_FRAME:AddMessage("debug1: " .. unit)
        --DEFAULT_CHAT_FRAME:AddMessage("debug2: " .. item)
        
        if (ref.coold > 0) then
            local time = GetTime()
            local r,g = 1, 0
            local realdur = ref.coold
            ref.duration = realdur

            for buffIndex=1,40 do
                local _,_,_,_,_,duration,eTime,_,_,_,sId = UnitBuff(unit, buffIndex)

                if (sId == spell) then
                    ref.duration = duration
                    realdur = duration
                    ref.etime = eTime
                    r,g = 255, 255
                    break
                end
            end

            ref.start=time
            ref.sid=spell
            self.onetime.barac[unit][item] = ref

            ref.cd:Show()
            ref.border:SetBackdropBorderColor(r, g, 0, 1)
            ref.cd:SetCooldown(time,realdur)
            self:BarText(ref.text,realdur)
            --self:DebugArch({["1UN"] = unit, ["2IT"] = item, ["3SP"] = spell, ["4CD"] = ref.coold, ["5AC"] = "Start"})
        end
        --ref.start = time
    end
end

function SimplyAlerts:StopTimer(ref, unit, item)
    if self.onetime.barac[unit][item] then self.onetime.barac[unit][item] = nil end
    --self:DebugArch({["2UN"] = unit, ["3IT"] = item, ["ASP"] = ref.sid, ["4AC"] = "Stop"})
    ref.border:SetBackdropBorderColor(0, 1, 0, 1)
    ref.text:SetText("")
    ref.cd:Hide()
    ref.start = 0
end

function SimplyAlerts:StopAllTimers()
    for unit,_ in pairs(self.db.profile.bars.barst) do
        if (self.db.profile.bars.barst[unit].enabled and self.icdBars[unit]) then
            for slot=12,15 do
                if (self.icdBars[unit][slot]) then
                    self:StopTimer(self.icdBars[unit][slot], unit, self.onetime.items[unit][slot])
                end
            end
        end
    end
end

function SimplyAlerts:BarText(text, cooldown)
    if (self.db.profile.bars.showtext) then
        if cooldown < 10 then 
            if cooldown <= 0.5 then
                text:SetText("")
            else
                text:SetFormattedText(" %d",cooldown)
            end
        else
            text:SetFormattedText("%d",cooldown)
        end

        if cooldown < 6 then
            text:SetTextColor(1,0,0,1)
        else 
            text:SetTextColor(1,1,0,1) 
        end
    end
end

function SimplyAlerts:BarTextUp(elapsed)
    for unit,_ in pairs(self.db.profile.bars.barst) do
        if (self.db.profile.bars.barst[unit].enabled and self.icdBars[unit]:IsShown()) then
            if (not self.total.barco[unit]) then self.total.barco[unit] = 0 end
            self.total.barco[unit] = self.total.barco[unit] + elapsed
            
            if self.total.barco[unit] > 0.25 then
                for item,ref in pairs(self.onetime.barac[unit]) do
                    local ptime = ref.duration

                    if ref.start + ptime - GetTime() <= 0 then
                        ptime = ref.coold
                    end

                    ptime = ref.start + ptime - GetTime()
                    if ptime <= 0 then
                        self:StopTimer(ref, unit, item)
                    else
                        if ref.etime and GetTime() >= ref.etime then
                            ref.border:SetBackdropBorderColor(1, 0, 0, 1)
                            ref.cd:SetCooldown(ref.start, ref.coold)
                        end
                        self:BarText(ref.text, floor(ptime+0.5))
                    end
                end
                
                self.total.barco[unit] = self.total.barco[unit] - 0.25
            end
        end
    end
end

function SimplyAlerts:CheckTrinkets(unit, save)
    if (string.find(unit, "party")) then
        if (CheckInteractDistance(unit, 1) and CanInspect(unit)) then
            NotifyInspect(unit)
        end
    end

    local fail = false
    self.onetime.items[unit] = {}

    for slot=13,14 do
        local itemLink = GetInventoryItemLink(unit, slot)

        if (itemLink) then
            if (save) then
                self.onetime.items[unit][slot] = self:ItemID(itemLink)
            elseif (string.find(unit, "party")) then
                self:UpdateIcon(unit, slot, self:ItemID(itemLink))
            end
        else
            fail = true
        end

        if (fail) then
            self.onetime.items[unit][slot] = 25561

            if (i == 14) then
                self.onetime.items[unit][12] = 25561
                self.onetime.items[unit][15] = 25561
            end

            fail = false 
        end
    end

    self:BonusIcons(unit)

    if (string.find(unit, "party") and save) then
        ClearInspectPlayer()
    end
end

function SimplyAlerts:ArenaBars(arg1)
    local _,_,_,_,_,teamSize,_ = GetBattlefieldStatus(1);
    if not teamSize then teamSize = 1; end

    
    for unitIndex=1,teamSize do 
        if (string.find(arg1, "The Arena battle has begun!")) then
            if (self.db.profile.bars.barst["arena" .. unitIndex].enabled and self.icdBars["arena" .. unitIndex]) then
                self.icdBars["arena" .. unitIndex]:Show()
            end

            self:UpdatePartyTrinkets(99, "party" .. unitIndex)
            self:UpdateFriendlyBonusIcons("party" .. unitIndex) 
        elseif (string.find(arg1, "Team wins!")) then
            if (self.icdBars["arena" .. unitIndex] and self.icdBars["arena" .. unitIndex]:IsShown()) then
                for slot=12,15 do
                    if (self.icdBars["arena" .. unitIndex][slot]) then
                        self:UpdateIcon("arena" .. unitIndex, slot, 25561)  
                    end
                end

                self.icdBars["arena" .. unitIndex][12]:Hide()
                self.icdBars["arena" .. unitIndex][15]:Hide()
                self.icdBars["arena" .. unitIndex]:Hide()
            end
        end
    end
end

function SimplyAlerts:PartyBars()
    for unitIndex=1,5 do
        if (self.icdBars["party" .. unitIndex] and self.db.profile.bars.barst["party" .. unitIndex].enabled) then
            if (UnitExists("party" .. unitIndex)) then
                self:UpdatePartyTrinkets(99, "party" .. unitIndex)
                self:UpdateFriendlyBonusIcons("party" .. unitIndex) 
                self.icdBars["party" .. unitIndex]:Show()
            else
                self.icdBars["party" .. unitIndex]:Hide()
            end
        end
    end
end

function SimplyAlerts:PlayerToUnit(name)
    local pos = string.find(name, "-")
    if (pos) then
        name = string.sub(name, 0, pos-1)
    end

    if (name == self.playerName) then
        return "player"
    end

    for unitIndex=1,5 do
        if (UnitExists("arena" .. unitIndex)) then
            local arenaPlayerName = UnitName("arena" .. unitIndex)
            if (arenaPlayerName == name) then
                return "arena" .. unitIndex
            end
        end

        if (UnitExists("party" .. unitIndex)) then
            local partyPlayerName = UnitName("party" .. unitIndex)
            if (partyPlayerName == name) then
                return "party" .. unitIndex
            end
        end
    end

    return false
end

function SimplyAlerts:GetItemCD(item)
    for k in pairs(self.ItemProcs) do
        if (self.ItemProcs[k].item[1] == item or self.ItemProcs[k].item[2] == item) then
            return self.ItemProcs[k].icd
        end
    end

    return 0
end

function SimplyAlerts:GetItemSlot(item)
    for slot=11,15 do
        local itemId = self:ItemID(GetInventoryItemLink("player", slot))

        if (itemId == item) then
            if (slot == 11) then slot = 12 end

            return slot
        end
    end

    return false
end

function SimplyAlerts:GetEnchantments()
    return {["3730"] = 55775, ["3722"] = 55637, ["3728"] = 55769}
end

function SimplyAlerts:IsItemAshenBand(itemId)
    local ashenBands = {
        [50398] = true, [50400] = true, [50402] = true,
        [50404] = true, [52572] = true
    }
    return ashenBands[itemId]
end

function SimplyAlerts:GetItemProcDuration(item, unit)
    for itemId in pairs(self.ItemProcs) do
        for itemIndex in pairs(self.ItemProcs[itemId].item) do
            if self.ItemProcs[itemId].item[itemIndex] == item then
                return select(6, UnitAura(unit, GetSpellInfo(itemId)))
            end
        end
    end

    return 0
end

function SimplyAlerts:GetProcItemID(spell, player)
    local unit = self:PlayerToUnit(player)

    if (unit) then
        if (string.find(unit, "party") or unit == "player") then
            for _,item in pairs(self.onetime.items[unit]) do
                for itemIndex=1,4 do
                    if (self.ItemProcs[spell].item[itemIndex] and item == self.ItemProcs[spell].item[itemIndex]) then
                        --DEFAULT_CHAT_FRAME:AddMessage("found - " .. self.ItemProcs[spell].item[itemIndex])
                        return self.ItemProcs[spell].item[itemIndex]
                    end
                end
            end
        else
            return self.ItemProcs[spell].item[1]
        end
    end
end

function SimplyAlerts:HandleItemStart(unit, slot, item, spell, slotCheck, fHide, fShow)
    if (string.find(unit, "arena") and self.onetime.items[unit][slot] == 25561 and self.onetime.items[unit][13] ~= item) then
        --DEFAULT_CHAT_FRAME:AddMessage("debug 8")
        self:UpdateIcon(unit, slot, item, fHide, fShow)
        --DEFAULT_CHAT_FRAME:AddMessage("debug 9")
    end

    if (self.onetime.items[unit][slot] == item) then
        --DEFAULT_CHAT_FRAME:AddMessage("debug 10")
        self:StartTimer(self.icdBars[unit][slot], unit, item, spell)
        --DEFAULT_CHAT_FRAME:AddMessage("debug 11")
    end
end

function SimplyAlerts:UpdatePlayerTrinkets(arg1, slot)
    if (slot == 13 or slot == 14) then
        local itemLink = GetInventoryItemLink("player", slot)

        if itemLink then
            local id, enchantId = itemLink:match("item:(%d+):(%d+):")
            id = tonumber(id)

            if (self.onetime.items.player[slot] ~= id and self.icdBars.player and id) then
                self:UpdateIcon("player", slot, id)
            end
        else
            local force = (slot == 15)
            self:UpdateIcon("player", slot, 25561, force)
        end
    end
end

function SimplyAlerts:UpdateFriendlyBonusIcons(unit)
    if (not self.db.profile.bars.barst[unit] or not self.icdBars[unit]) then
        return
    end

    if (self.db.profile.bars.barst[unit].bonuses) then
        --DEFAULT_CHAT_FRAME:AddMessage("-- UpdateFriendlyBonusIcons " .. unit)
        if (string.find(unit, "party")) then
            NotifyInspect(unit)
        end

        local success = false

        -- ashen
        for slot=11,12 do
            local itemId = self:ItemID(GetInventoryItemLink(unit, slot))

            if (itemId) then
                --DEFAULT_CHAT_FRAME:AddMessage("id == true")
                if (self:IsItemAshenBand(itemId)) then
                    self:UpdateIcon(unit, 12, itemId)
                    success = true

                    break
                end
            end
        end

        if success then
            self.icdBars[unit][12]:Show()
            success = false
            --DEFAULT_CHAT_FRAME:AddMessage("-- 1 success")
        else
            self.icdBars[unit][12]:Hide()
            --DEFAULT_CHAT_FRAME:AddMessage("-- 1 fail")
        end

        -- enchant
        local itemLink = GetInventoryItemLink(unit, 15)

        if itemLink then
            local _, enchantId = itemLink:match("item:(%d+):(%d+):")
            local enchants = self:GetEnchantments()

            if (enchants[enchantId]) then
                --DEFAULT_CHAT_FRAME:AddMessage("enchant: " .. enchants[enchantId])
                self:UpdateIcon(unit, 15, enchants[enchantId])
                self.icdBars[unit][15]:Show()
                success = true
            end
        end

        if success then
            self.icdBars[unit][15]:Show()
            success = false
            --DEFAULT_CHAT_FRAME:AddMessage("-- 2 success")
        else
            self.icdBars[unit][15]:Hide()
            --DEFAULT_CHAT_FRAME:AddMessage("-- 2 fail")
        end

        if (string.find(unit, "party")) then
            ClearInspectPlayer()
        end
    end
end

function SimplyAlerts:UpdatePartyTrinkets(time, arg1)
    if (string.find(arg1, "party") and self.icdBars[arg1]) then
        self:CheckTrinkets(arg1, false)

        for slot=13,14 do
            local itemLink = GetInventoryItemLink(arg1, slot)

            if (itemLink) then
                local itemid = self:ItemID(itemLink)
                
                if (self.onetime.items[arg1][slot] ~= itemid) then
                    -- DEFAULT_CHAT_FRAME:AddMessage("updating " .. arg1 .. " to " .. itemId)
                    self:UpdateIcon(arg1, slot, itemid)
                end
            end
        end

        ClearInspectPlayer()
    end
end