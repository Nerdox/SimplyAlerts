-----------------------------------------------------
--               SPELL STATUS COUNTER              --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:CountSpell(missType, spell, player, target)
    if (self.db.profile.spellstatus.filter[missType] and GetSpellInfo(spell) and self:IsArena()) then
        for who in pairs(self.db.profile.spellstatus.reporter.types) do
            if ((who == "enemy" and player ~= self.playerName and target == self.playerName and self.db.profile.spellstatus.reporter.types["enemy"]) 
            or (who == "me" and player == self.playerName and target ~= self.playerName and self.db.profile.spellstatus.reporter.types["me"])) then
                if (not self.onetime.counters[who][missType]) then
                    self.onetime.counters[who][missType] = { }
                end

                if (self.onetime.counters[who][missType][spell]) then
                    self.onetime.counters[who][missType][spell] = self.onetime.counters[who][missType][spell]+1
                else
                    self.onetime.counters[who][missType][spell] = 1
                end
            end
        end
    end
end

function SimplyAlerts:SpellReset()
    for who in pairs(self.db.profile.spellstatus.reporter.types) do
        self.onetime.counters[who] = {}
    end
end

function SimplyAlerts:SpellMessage(msg)
    if (self.db.profile.spellstatus.reporter.chat == "me") then
        DEFAULT_CHAT_FRAME:AddMessage(msg)
    else
        C_Timer.After(1, function () SendChatMessage(msg, self.db.profile.spellstatus.reporter.chat) end)
    end
end

function SimplyAlerts:SpellReport(arg1)
    if (string.find(arg1, "Team wins!") and self.db.profile.spellstatus.reporter.enabled) then
        self:SpellMessage("-\\/ SimplyAlerts Report \\/-")

        for who in pairs(self.db.profile.spellstatus.reporter.types) do
            for missType,_ in pairs(self.db.profile.spellstatus.filter) do
                if (self.onetime.counters[who][missType] and self.db.profile.spellstatus.reporter.types[who]) then
                    if (who == "enemy") then
                        self:SpellMessage("-- " .. missType .. " ON ME --")
                    elseif (who == "me") then
                        self:SpellMessage("-- " .. missType .. " BY ME --")
                    end

                    local isFirst, output = true, ""
                    
                    for spellId,missCounter in pairs(self.onetime.counters[who][missType]) do
                        if isFirst then
                            output = self:SpellName(spellId) .. " - " .. missCounter

                            isFirst = false
                        else
                            output = output .. " /\\ " .. self:SpellName(spellId) .. " - " .. missCounter
                        end
                    end

                    self:SpellMessage(output)
                end
            end
        end
        self:SpellReset()
    end
end