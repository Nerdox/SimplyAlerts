-----------------------------------------------------
--             ARENA FRIENDLIST DODGE             --
-----------------------------------------------------

local self = SimplyAlerts

function SimplyAlerts:IsZoneArena(zone)
    local arenas = {
        'Ruins of Lordaeron',
        'The Ring of Valor',
        'Nagrand Arena',
        'Dalaran Arena',
    }

    for _, arena in ipairs(arenas) do
        if (zone == arena) then
            return true
        end
    end

    return false
end

function SimplyAlerts:GetNumOnlineFriends()
    local _, onlineFriends = GetNumFriends()
    
    return onlineFriends
end

function SimplyAlerts:LeaveAllQueues()
    for queueIndex=1, MAX_BATTLEFIELD_QUEUES do
        status = GetBattlefieldStatus(queueIndex)

        if (status == "queued") then
            AcceptBattlefieldPort(queueIndex, false)
        end
    end

    self:AddMessage(" Dodger", "Leaved all queues")
end

function SimplyAlerts:ControlFriendlist()
    ShowFriends()
    local onlineFriends = self:GetNumOnlineFriends()

    if (onlineFriends > 0) then
        local leaveQueue = true

        for friendIndex=1, onlineFriends do 
            name, _, _, area, _, status, note = GetFriendInfo(friendIndex);

            if (not self:IsZoneArena(area)) then
                local leaveQueue = true

                if (self.db.profile.dodger.note.enabled) then
                    if (not note or note and self.db.profile.dodger.note.string ~= note) then
                        leaveQueue = false
                    end
                end

                if (status == "<AFK>") then
                    if (self.db.profile.dodger.ignoreafk) then
                        leaveQueue = false
                    end
                end

                        
                if (leaveQueue) then
                    self:AddMessage(" Dodger", name .. " has been dodged")
                    self:LeaveAllQueues()
                    break
                end
            end
        end
    end
end

function SimplyAlerts:InitDodger()
    if (self.db.profile.dodger.enabled) then
        if (self:IsArena()) then
            self:CancelTimer(self.timer)
        else
            if (not self.timer or not self:TimeLeft(self.timer)) then
                self.timer = self:ScheduleRepeatingTimer("ControlFriendlist", 1)
                self:AddMessage(" Dodger", "Inited dodging")
            end
        end
    end
end

function SimplyAlerts:ForceStopTimer()
    self:CancelTimer(self.timer)
end