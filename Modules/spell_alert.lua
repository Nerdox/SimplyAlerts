-----------------------------------------------------
--                 Alert Functions                 --
-----------------------------------------------------

local L, SM = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true), LibStub("LibSharedMedia-3.0")
local self = SimplyAlerts

function SimplyAlerts:SavePosition()
    self.db.profile.alert.pos = {alert:GetPoint()}
end

function SimplyAlerts:CreateFrame()
    local alert = CreateFrame("Frame", "SimplyAlertsAlert", UIParent)
    alert:SetWidth(1) 
    alert:SetHeight(1) 
    alert:SetAlpha(.90);
    alert:SetPoint("CENTER")
    alert:EnableMouse(true)
    alert:SetMovable(true)
    alert.text = alert:CreateFontString(nil,"ARTWORK") 
    alert.text:SetFont(SM:Fetch("font"), 40)
    alert.text:SetPoint("CENTER", 0, 0)

    alert:SetScript("OnMouseDown", function(self) self:StartMoving() end)
    alert:SetScript("OnMouseUp", function(self) self:StopMovingOrSizing(); SimplyAlerts:SavePosition(self) end)
    alert:SetScript("OnUpdate", function(self,elapsed)
        self.elapsed = self.elapsed + elapsed
        if (self.elapsed <= 0.1) then
            self:SetAlpha(1-((0.1-self.elapsed)/0.1))
        elseif (self.elapsed <= 0.1 + SimplyAlerts.db.profile.alert.holdtime) then
            self:SetAlpha(1)
        elseif (self.elapsed <= 0.2 + SimplyAlerts.db.profile.alert.holdtime) then
            self:SetAlpha((SimplyAlerts.db.profile.alert.holdtime + 0.2 - self.elapsed)/0.1)
        else
            self:SetAlpha(0)
            self:Hide()
        end
    end)

    alert:Hide()

    return alert
end

function SimplyAlerts:UpdateFrame()
    self.alert.text:SetFont(SM:Fetch("font", self.db.profile.alert.font), self.db.profile.alert.fontsize, "THICKOUTLINE")
    self.alert:SetPoint(unpack(self.db.profile.alert.pos))

    if (self.db.profile.alert.lock) then
        self.alert:EnableMouse(false)
        self.alert:SetMovable(false)
        self.alert:SetWidth(1)
        self.alert:SetHeight(1)
    else
        self.alert:EnableMouse(true)
        self.alert:SetMovable(true)
        self.alert:SetWidth(100)
        self.alert:SetHeight(20)
    end
end
 
function SimplyAlerts:ShowAlert(msg)
    self.alert:Hide()
    self.alert.elapsed = 0
    self.alert.text:SetText(msg)
    self.alert:Show()
end