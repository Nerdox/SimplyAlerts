-----------------------------------------------------
--                    STATUS BAR                   --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:GetStatusBarSettings(unit)
    if string.find(unit, "party") then
        unit = "party"
    end

    return self.db.profile.statusBars.barTypes[unit] or false
end

function SimplyAlerts:UpdateStatusBar(frame)
    if not UnitExists(frame.unit) or not frame.TextString or frame.SAHovered then
        return
    end

    local settings = self:GetStatusBarSettings(frame.unit)
    local currentValue, minValue, maxValue = frame:GetValue(), frame:GetMinMaxValues()

    if not self.db.profile.statusBars.enabled or not settings or not settings.enabled then
        if frame.SALeftTextString and frame.SALeftTextString:IsVisible() then
            frame.TextString:Show()
            frame.SALeftTextString:Hide()
            frame.SARightTextString:Hide()
        end

        return
    end

    if not frame.SALeftTextString then
        frame.SALeftTextString = self:CreateStatusTextFrame(frame, "LeftStatusBarText")
        frame.SARightTextString = self:CreateStatusTextFrame(frame, "RightStatusBarText")
        self:UpdateStatusBarPoints(frame)
        self:InitHoverScripts(frame, settings)
    end

    local unitIsDeadOrGhost = UnitIsDeadOrGhost(frame.unit)

    if frame.powerType and not settings.powers[frame.powerType] or unitIsDeadOrGhost then
        frame.SALeftTextString:Hide()
        frame.SARightTextString:Hide()
        
        if self.db.profile.statusBars.updateDisplay and not unitIsDeadOrGhost then
            frame.TextString:SetText(currentValue)
            frame.TextString:Show()
        end

        return
    end

    frame.TextString:Hide()
    frame.SALeftTextString:SetText(math.floor(currentValue / maxValue * 100) .. "%")
    frame.SARightTextString:SetText(currentValue)

    if not frame.SALeftTextString:IsVisible() then
        frame.SALeftTextString:Show()
        frame.SARightTextString:Show()
    end
end

function SimplyAlerts:CreateStatusTextFrame(frame, newFrameName)
    local tempFrame = CreateFrame("frame", newFrameName, frame)
    tempFrame:SetFrameLevel(frame:GetFrameLevel() + 3)

    tempFrame = tempFrame:CreateFontString(nil, "OVERLAY")
    tempFrame:SetFontObject(frame.TextString:GetFontObject())
    tempFrame:SetSpacing(frame.TextString:GetSpacing())
    tempFrame:SetTextColor(frame.TextString:GetTextColor())

    return tempFrame
end

function SimplyAlerts:UpdateStatusBarPoints(frame)
    if not frame.SALeftTextString then
        return Frame
    end

    local settings = self:GetStatusBarSettings(frame.unit)
    local _, relativeTo, relativePoint, xOfs, yOfs = frame.TextString:GetPoint()

    frame.SALeftTextString:SetPoint("LEFT", relativeTo, relativePoint, xOfs - settings.offPerc, yOfs)
    frame.SARightTextString:SetPoint("RIGHT", relativeTo, relativePoint, xOfs + settings.offPower, yOfs)

    return frame
end

function SimplyAlerts:InitHoverScripts(frame, settings)
    local initialStatusPercentage = GetCVar("statusTextPercentage")
    frame.SAHovered = false

    frame:SetScript("OnEnter", function()
        self:ToggleHoverText(frame, initialStatusPercentage)
    end)

    frame:SetScript("OnLeave", function()
        self:ToggleHoverText(frame, initialStatusPercentage)
    end)
end

function SimplyAlerts:ToggleHoverText(frame, initialStatusPercentage)
    local settings = self:GetStatusBarSettings(frame.unit)

    if not frame.powerType or settings.powers[frame.powerType] then
        SetCVar("statusTextPercentage", not initialStatusPercentage)
        frame.SAHovered = not frame.SAHovered

        if frame.TextString:IsVisible() then
            frame.TextString:Hide()
            frame.SALeftTextString:Show()
            frame.SARightTextString:Show()
        else
            local currentValue, _, maxValue = frame:GetValue(), frame:GetMinMaxValues()
            
            frame.TextString:SetText(currentValue .. " / " .. maxValue)
            frame.TextString:Show()
            frame.SALeftTextString:Hide()
            frame.SARightTextString:Hide()
        end
    end
end
