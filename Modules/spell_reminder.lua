-----------------------------------------------------
--                  SPELL REMINDER                 --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:Reminder(elapsed)
    if (self.db.profile.reminder.enabled) then
        self.total.rmnd = self.total.rmnd + elapsed
        if (self.total.rmnd >= self.db.profile.reminder.interval) then
            if (next(self.onetime.reminders) ~= nil) then
                local i=1
                for k,v in ipairs(self.onetime.reminders) do
                    i=i+1

                    if (self.onetime.reminders[1] == v and self.onetime.lastmind == 1) then
                        self:RemindMe(v)
                    else
                        if (self.onetime.reminders[self.onetime.lastmind] == v) then
                            self:RemindMe(v)

                            if (self.onetime.reminders[self:LastValue(self.onetime.reminders)] == v) then
                                self.onetime.lastmind = 1
                            end
                        else
                            self.onetime.lastmind = i
                        end
                    end
                end
            end
            self.total.rmnd = 0
        end
    end
end

function SimplyAlerts:RemindMe(spell)
    local name, rank, icon = GetSpellInfo(spell)
    if (self.db.profile.licon and icon) then icon = "|h|T" .. icon .. ":" .. (self.db.profile.licsiz+1) .. ":" .. (self.db.profile.licsiz+1) .. "|t|h"
    else icon = "" end

    if (self.db.profile.uppercase["local"] == "dsonly" or self.db.profile.uppercase["local"] == "bothe") then name = string.upper(name or "") end
    local str = self.db.profile.messages["lmsgsdw"]
    if (string.find(str, "%s")) then str = string.replace(str, "%s", name) end

    self:ShowAlert(icon..str..icon)
end

function SimplyAlerts:SetRemind(spell)
    if (self:IsArena() and self.db.profile.onlyarena.reminder or not self.db.profile.onlyarena.reminder) then
        table.insert(self.onetime.reminders, spell)
    end
end

function SimplyAlerts:UnRemind(spell)
    table.remove(self.onetime.reminders, spell)
end