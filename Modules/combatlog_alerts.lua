-----------------------------------------------------
--                 COMBATLOG ALERTS                --
-----------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("SimplyAlerts", true)
local self = SimplyAlerts

function SimplyAlerts:GetInfoMsg(spell, state, dspell, player, me)
    if (self.db.profile.localmsgs and ((self.db.profile.onlyarena["local"] and self:IsArena()) or (not self.db.profile.onlyarena["local"]))) then
        for _, spellId in ipairs(self.db.profile.Spellz) do
            if (spellId == spell or spellId == dspell) then
                local name, _, icon = GetSpellInfo(spell)
                if (dspell and string.match(dspell, "%d")) then
                    name, _, icon = GetSpellInfo(dspell)
                end

                if not name then
                    break
                end

                icon = self.db.profile.licon and icon and ("|h|T" .. icon .. ":" .. (self.db.profile.licsiz + 1) .. ":" .. (self.db.profile.licsiz + 1) .. "|t|h") or ""
    
                if (self.db.profile.uppercase["local"] ~= "bothd") then
                    name = string.upper(name)
                end
    
                if (player == self.playerName and (state == "SPELL_DISPEL" or state == "SPELL_STOLEN")) then
                    local str = self.db.profile.messages["lmsgsds"]:gsub("%%s", name)
                    self:ShowAlert(icon .. str .. icon)
                elseif (state == "SPELL_AURA_REMOVED" and me == self.playerName) then
                    local str = self.db.profile.messages["lmsgsdw"]:gsub("%%s", name)
                    self:ShowAlert(icon .. str .. icon)
                end
                break
            end
        end
    end

    if (self.db.profile.chatmsgs and ((self.db.profile.onlyarena["chat"] and self:IsArena()) or (not self.db.profile.onlyarena["chat"]))) then
        for _, spellId in ipairs(self.db.profile.Spellzp) do
            if (spellId == spell or spellId == dspell) then
                if (player == self.playerName and (state == "SPELL_DISPEL" or state == "SPELL_STOLEN")) then
                    SendChatMessage("{SKULL} " .. me .. " dispelled " .. self:SpellName(dspell) .. " (" .. self:SpellName(spell) .. ") {SKULL}", self.db.profile.chat)
                elseif (me == self.playerName) then
                    local sname = self:SpellName(spell) or ""
                    if (self.db.profile.uppercase["chat"] ~= "bothd") then 
                        sname = string.upper(sname)
                    end
    
                    if (state == "SPELL_AURA_APPLIED" or state == "SPELL_AURA_REFRESH") then
                        local str = self.db.profile.messages["gmsgup"]:gsub("%%s", sname)
                        SendChatMessage(str, self.db.profile.chat)
                    elseif (state == "SPELL_AURA_REMOVED") then
                        local str = self.db.profile.messages["gmsgdw"]:gsub("%%s", sname)
                        SendChatMessage(str, self.db.profile.chat)
                    end
                end
                break
            end
        end
    end
end

function SimplyAlerts:GetControlMsg(spell)
    if (self.db.profile.controls and self.SpellCon[spell]) then
        SendChatMessage(self.SpellCon[spell], self.db.profile.chat)
    end
end

function SimplyAlerts:GetExtraMsgs(spell, missType, player, destPlayer)
    if not self.db.profile.spellstatus.filter[missType] then
        return
    end

    if (self.db.profile.spellstatus["enabled"]) then
        if (self.db.profile.onlyarena["spell"] and self:IsArena() or not self.db.profile.onlyarena["spell"]) and ((
            ((self.db.profile.spellstatus["type"] == "me" or self.db.profile.spellstatus["type"] == "meparty") and (self.playerName == player or self.playerName == destPlayer))
            or ((self.db.profile.spellstatus["type"] == "party" or self.db.profile.spellstatus["type"] == "meparty") and (self:IsPartyMember(player) or self:IsPartyMember(destPlayer)))
            or self.db.profile.spellstatus["type"] == "all"
        )) then
            SendChatMessage(destPlayer .. " failed " .. self:SpellName(spell) .. " on " .. player .. ": " .. missType, self.db.profile.chat)
        end
   end
end