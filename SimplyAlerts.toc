## Interface: 30300
## Title: SimplyAlerts
## Version: v1.1
## Notes: says to party/yourself alerts about your control/main cds
## Author: Nerdox
## X-eMail: dominoox97 at gmail dot com
## X-Category: Battlegrounds/PvP, Chat/Communication
## DefaultState: Enabled
## SavedVariables: SimplyAlertsDB

embeds.xml

Locale\enUS.lua

SimplyAlerts.lua
config.lua
options.lua

# Modules\auto_rbgs.lua # Disabled due to restriction of WoW Lua
Modules\combatlog_alerts.lua
# Modules\friendlist_dodge.lua # Disabled due to restriction of WoW Lua
Modules\icd_tracker.lua
Modules\mirror_functions.lua
Modules\spell_alert.lua
Modules\spell_reminder.lua
Modules\spell_status_counter.lua
Modules\status_bars.lua